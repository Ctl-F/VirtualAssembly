#include "parsing.h"

vector<token> parse_asm(string feed) {
	vector<token> tokens;

	while(feed.length() > 0) {
		if(parse_whitespace(feed)) continue;
		if(parse_comment(feed)) continue;

		if(feed[0] == ';') {
			token t;
			t.type = TYPE_STRING;
			t.tok_value = ";";
			tokens.push_back(t);
			feed = feed.replace(0, 1, "");
			continue;
		}

		if(feed[0] == '+' || feed[0] == '-') {
			token t;
			t.type = TYPE_OPERATOR;
			t.tok_value = (feed[0] == '+' ? "+" : "-");
			tokens.push_back(t);
			feed = feed.replace(0, 1, "");
		}

		token t = parse_directive(feed);
		if(t.type != TYPE_NONE) {
			tokens.push_back(t);
			continue;
		}

		t = parse_number(feed);
		if(t.type != TYPE_NONE) {
			tokens.push_back(t);
			continue;
		}

		t = parse_hex(feed);
		if(t.type != TYPE_NONE) {
			tokens.push_back(t);
			continue;
		}

		t = parse_identifier(feed);
		if(t.type != TYPE_NONE) {
			tokens.push_back(t);
			continue;
		}

		// if we get to this point, we have a character that doesn't fit the description of any of the usable tokens and we throw an error
		printf("Parsing error, Stub:\n'%s...(%i)'\n%c: %i\nNumber of Tokens Already Parsed: %i\n", feed.substr(0, MIN(feed.length(), 25)).c_str() , feed.length(), feed[0], feed[0], tokens.size());
		tokens.clear();
		return tokens;
	}
	return tokens;
}

void merge_span(token *tokens, unsigned int length) {
	for(unsigned int i = 0; i < length - 3; i++) {
		if(tokens[i].type == TYPE_STRING && tokens[i + 1].type == TYPE_OPERATOR && (tokens[i + 2].type == TYPE_NUMBER || tokens[i+2].type == TYPE_STRING)) {

		}
	}
}

token parse_directive(string &feed) {
	string tok = ".";
	if(feed[0] != '.') {
		token ret;
		ret.type = TYPE_NONE;
		return ret;
	}

	for(unsigned int i = 1; i < feed.length(); i++) {
		char c = feed[i];
		if(c == ' ' || c == 13 || c == 10 || c == 9 || c == ';') {
			break;
		}
		tok += c;
	}

	token ret;
	ret.tok_value = tok;
	ret.type = TYPE_DIRECTIVE;
	feed = feed.replace(0, tok.length(), "");
	return ret;
}

token parse_identifier(string &feed) {
	string tok = "";
	string allowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789";
	for(unsigned int i = 0; i < feed.length(); i++) {
		char c = feed[i];
		if(allowed.find(c) != string::npos) {
			tok += c;
		}
		else {
			break;
		}
	}
	if(tok.length() > 0) {
		if(!IS_NUMERIC(tok[0])) {
			token ret;
			ret.type = TYPE_STRING;
			ret.tok_value = tok;
			feed = feed.replace(0, tok.length(), "");

			if(feed[0] == '[') {
				// array
				feed = feed.erase(0, 1);
				ret.is_array = true;
				ret.tok_value += "[";

				if(feed[0] == ']') {
					ret.array_index = "";
					ret.array_index_type = TYPE_NONE;
					feed = feed.erase(0, 1);
					return ret;
				}
				else if(IS_NUMERIC(feed[0]) || feed[0] == '.' || feed[0] == '-') {
					token _t = parse_number(feed);
					if(feed[0] == ']') {
						ret.array_index = _t.tok_value;
						ret.array_index_type = TYPE_NUMBER;
						feed = feed.erase(0, 1);
					}
					else {
						// some sort of error in the array indexing
						cout << "Array indexing error: " << feed[0] << " in array " << ret.tok_value << endl;
						ret.type = TYPE_NONE;
						return ret;
					}
				}
				else {
					token _t = parse_identifier(feed);
					if(_t.type == TYPE_STRING) {
						if(_t.is_array) {
							cout << "Array as array index is not allowed: " << ret.tok_value << _t.tok_value << endl;
							ret.type = TYPE_NONE;
							return ret;
						}
						if(feed[0] == ']') {
							ret.array_index = _t.tok_value;
							ret.array_index_type = TYPE_STRING;
							feed = feed.erase(0, 1);
						}
						else {
							cout << "Array indexing error: " << feed[0] << " in array " << ret.tok_value << endl;
							ret.type = TYPE_NONE;
							return ret;
						}
					}
					else {
						cout << "Directive as array index is not allowed: " << ret.tok_value << _t.tok_value << endl;
						ret.type = TYPE_NONE;
						return ret;
					}
				}
			}

			return ret;
		}
	}
	token ret;
	ret.type = TYPE_NONE;
	return ret;
}

token parse_number(string &feed) {
	string tok = "";
	string allowed = "-0.123456789";
	bool doubleneg = false, doubledot = false;
	for(unsigned int i = 0; i < feed.length(); i++) {
		char c = feed[i];
		if(allowed.find(c) != string::npos) {
			if(c == '.') {
				if(doubledot) {
					break;
				}
				doubledot = true;
			}
			else if(c == '-') {
				if(doubleneg) {
					break;
				}
				doubleneg = true;
			}
			tok += c;
		}
		else {
			break;
		}
	}

	if(tok.length() > 0) {
		token ret;
		ret.type = TYPE_NUMBER;
		ret.tok_value = tok;
		ret.dvalue = atof(tok.c_str());
		feed = feed.replace(0, tok.length(), "");
		return ret;
	}
	token ret;
	ret.type = TYPE_NONE;
	return ret;
}

token parse_hex(string &feed) {
	string tok = "", allowed = "0123456789ABCDEFabcdef";
	if(feed[0] != '$') {
		token ret;
		ret.type = TYPE_NONE;
		return ret;
	}

	for(unsigned int i = 1; i < feed.length(); i++) {
		char c = feed[i];
		if(allowed.find(c) != string::npos) {
			tok += c;
		}
		else {
			break;
		}
	}

	if(tok.length() > 0) {
		token ret;
		ret.type = TYPE_NUMBER;
		unsigned long n;
		stringstream ss;
		ss << hex << tok;
		ss >> n;
		ret.dvalue = (double) n;
		ret.tok_value = "$" + tok;
		feed = feed.replace(0, tok.length() + 1, "");
		return ret;
	}
	token ret;
	ret.type = TYPE_NONE;
	return ret;
}

bool parse_whitespace(string &feed) {
	string tok = "";
	for(unsigned int i = 0; i < feed.length(); i++) {
		char c = feed[i];
		if(c == ' ' || c == 13 || c == 10 || c == 9) {
			tok += c;
		}
		else {
			break;
		}
	}
	if(tok.length() > 0) {
		feed = feed.replace(0, tok.length(), "");
		return true;
	}
	return false;
}

bool parse_comment(string &feed) {
	string tok = "#";
	if(feed[0] != '#') {
		return false;
	}
	for(unsigned int i = 1; i < feed.length(); i++) {
		char c = feed[i];
		tok += c;
		if(c == '#') break;
	}
	if(tok.length() > 0) {
		feed = feed.replace(0, tok.length(), "");
		return true;
	}
	return false;
}