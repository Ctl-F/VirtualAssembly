#pragma once

#include <string>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <iostream>

using std::vector;
using std::string;
using std::atof;
using std::cout;
using std::endl;
using std::stringstream;
using std::hex;

#define TYPE_STRING 0
#define TYPE_NUMBER 1
#define TYPE_DIRECTIVE 2
#define TYPE_NONE 3
#define TYPE_OPERATOR 4

#define IS_NUMERIC(a) (48 <= a && a < 58)
#define MIN(a, b) (a<b?a:b)

struct token{
	unsigned char type = TYPE_NONE; // type of token
	bool is_array = false; // is the token an array? (this hasn't been implemented, and may not ever be)
	string tok_value = ""; // the actual string representation of the token
	unsigned char cmdval = 255; // the integer command value of the token, see command.h for more details
	unsigned int jmpto = -1; // next token index to perform (if,jmp,call have far jump to indexs, def will have a jump to of *this_index+3;
	
	bool offset_real;
	token *offset_tok;
	unsigned long base_addr; //base variable address
	unsigned short *dvalue;

	bool operator==(string s) { // more convenient token == syntax rather than token.tok_value == s
		return tok_value == s;
	}
	bool operator==(char *s) {
		return tok_value == s;
	}
	bool operator==(const char *s) {
		return tok_value == s;
	}
	bool operator==(unsigned char code) { // check against cmd value
		return cmdval == code;
	}
	bool operator==(int code) { // check against cmd value
		return cmdval == code;
	}
};

// this is the blanket function call. The other functions are specialized to parse their respective tokens from
// the begining of the string using the longest token rule
vector<token> parse_asm(string feed); 

// perform AFTER everything is finalized
void merge_span(token *tokens, unsigned int length);

// use the longest token rule to parse their respective tokens from the begining of the string
token parse_directive(string &feed);
token parse_identifier(string &feed);
token parse_number(string &feed);
token parse_hex(string &feed);
bool parse_whitespace(string &feed);
bool parse_comment(string &feed);