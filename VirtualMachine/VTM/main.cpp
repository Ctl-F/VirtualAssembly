#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <stack>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <cmath>
#include <time.h>
#include <thread>
#include <atomic>

#include <allegro5\allegro.h>
#include <allegro5\allegro_primitives.h>

#include "command.h"
#include "parsing.h"

using namespace std;
using namespace std::chrono;

//#define STD_IMPORT_PATH ".\\import\\"

// use with integers to get a 1-dimensional index from a 2-dimensional index
#define INDEX(a, b, w) (b*w+a)

// allegro clock frame rate
#define FPS 120.0

// the first three tokens in any execution module need to be the width, height, and scale
#define TOKEN_OFFSET 3

// These macros are used for the validate_* functions they are used as bitmasks to describe
// the allowed kind of argument to a command
#define ARGUMENT_NUMBER 0b0001
#define ARGUMENT_VARIABLE 0b0010
#define ARGUMENT_DIRECTIVE 0b0100

// error codes (for print_err and error checking)
#define ERROR_SEMICOLON 0
#define ERROR_INVALID_ARGUMENT 1
#define ERROR_INVALID_COMMAND 2
#define ERROR_NOERROR 3
#define ERROR_UNKNOWN_VARIABLE 4
#define ERROR_UNKNOWN_DIRECTIVE 5
#define ERROR_OTHER 6

// throw_error macro - for use in functions solely
#define THROW_ERROR() do { run_loop = false; exec = false; return; } while(0);

// map iterator types
typedef unordered_map<string, double>::iterator variable;
typedef unordered_map<string, token>::iterator constant;
typedef unordered_map<string, unsigned int>::iterator label;
typedef unsigned int uint;

// global variables
string local_import_path, STD_IMPORT_PATH, usercode;
token *tokens;
uint token_size, tindex, *pixels;
int width, height, scale, interupt_count = 0;
bool suspend = false, run_loop = true, exec = true;
long delay = 0;
bool variable_access_flag = false; // true if bad variable access

bool key_states[ALLEGRO_KEY_MAX];

ALLEGRO_DISPLAY *main_window;
ALLEGRO_EVENT_QUEUE *main_eq;
ALLEGRO_TIMER *timer;

unordered_map<string, double> variables; // user variables
unordered_map<string, token> constants; // constants (we don't really HAVE to keep a record of constants, but this is here anyway in case I want to add something along the lines of dynamic importing)
unordered_map<string, unsigned int> label_op; // label jump positions
stack<uint> call_stack; // function call stack

string get_path(string fname);

// these validate commands are here to make sure the the user's code is syntactically valid.
// this is an attempt to minimize the amount of error checking that must be done when executing
// the commands. The arg_type is for use with the bitmasks so for a multi-type argument use the 
// bitwise or '|' operator. Any type would be ARGUMENT_NUMBER|ARGUMENT_VARIABLE|ARGUMENT_DIRECTIVE
// another step taken care of in the validate_tokens function is the saving of label indexs so
// we don't have to iterate in order to jump. This also saves the end indexs of each if command
// so we don't have to iterate over the body of an if statement in the event of a false condition
// this action makes it so that it doesn't matter the size of an if statement nor how far we must
// jump away from our current position. All jumps, function calls, and if statements will execute
// at the same speed respectively regardless of the actual distance of the call. this function
// performs another optimization. In command.h every command is given an integer code. validate_tokens
// assigns each integer code to each token so when the time comes to determine which command function
// to call we can use quick integer comparison instead of slow string comparison
// start_index = command_index + 1;
byte validate_command_noarg(uint start_index);
byte validate_command_1arg(uint start_index, byte arg_type);
byte validate_command_2arg(uint start_index, byte arg_type1, byte arg_type2);
byte validate_command_3arg(uint start_index, byte arg_type1, byte arg_type2, byte arg_type3);
bool validate_tokens();

// return the current time in nanoseconds
inline long current_time();
inline long current_time_ms();

// returns an iterator from the variables map that corresponds to the variable name
// if there was an error accessing the variable (e.g. if the variable doesn't exist)
// it sets the variable_access_flag to true and returns an iterator that equals
// variables.end()
variable get_variable(string vname);

// search for jump indexs, this function has been depreciated by the validate_tokens
// function
void find_jump_points();

// replaces all .import ...; directives with the contents of the file provided
// this step is crucial because .import ...; will likely throw an error in the
// parser if left undone. This also allows for std:... filenames and .\ filenames
// a std:... filename will import a file from the library folder (.\import\) directory
// from the virtual machine. These kinds of imports don't use the extension as it is
// implied to be a .a (all std libraries will be a .a) a .\ filename uses 
// the local directory OF THE USER FILE and REQUIRES the extension to be present
// IF the extension exists in the first place. All other filenames given
// that don't have .\ syntax nor std: syntax are taken as the full qualified file path
void handle_imports(string &feed);

// handles the rendering from the pixel* map to the actual display window
void render();

void execute();

// functions correlated to each command that can be called from the user asm
// a real_value can be either a variable_name or a number literal
void fdef();          // def [variable_name]; define variable
void finc();          // inc [variable_name]; increment variable (variable++)
void fdec();          // dec [variable_name]; decrement variable (variable--)
void fconst();        // const [constant_name] [token]; replaces all appearances of [constant_name] with the given [token], there are no guards against redefining command names
void fld();           // ld [variable_name] [real_value]; stores the given value to the variable (variable = value)
void fadd();          // add [variable_name] [real_value_a] [real_value_b]; adds real_value_a and real_value_b then stores the result in the given variable (variable = real_value_a + real_value_b);
void fsub();          // sub [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a - real_value_b);
void feq();           // eq [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a == real_value_b);
void fneq();          // neq [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a != real_value_b);
void fprint();        // print [real_value]; prints the real_value to the console with a newline attached (printf("%d\n", real_value))
void fputc();         // putc [real_value]; prints the given ascii character to the console
void inline fflush(); // DEPRECIATED: In the past this would need to be called to flush all the values of putc to the console, now it's just a noop
void flth();          // lth [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a < real_value_b);
void fgth();          // gth [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a > real_value_b);
void fmul();          // mul [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a * real_value_b);
void fdiv();          // div [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a / real_value_b);
void fmod();          // mod [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a % real_value_b);
void flshift();       // lshift [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a << real_value_b);
void frshift();       // rshift [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a >> real_value_b);
void flor();          // or [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a || real_value_b); Note the l is short for logical, in the actual user code it's just or
void fland();         // and [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a && real_value_b);
void flxor();         // xor [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a ^^ real_value_b);
void fband();         // band [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a & real_value_b); Note the b is short for binary, the b is present in the user command
void fbor();          // bor [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a | real_value_b);
void fbxor();         // bxor [variable_name] [real_value_a] [real_value_b]; (variable = real_value_a ^ real_value_b);
void flnot();         // not [variable_name] [real_value]; (variable = !real_value);
void fbnot();         // bnot [variable_name] [real_value]; (variable = ~real_value);
void fsqrt();         // sqrt [variable_name] [real_value]; (variable = sqrt(real_value))
void fsin();          // sin [variable_name] [real_value]; (variable = sin(real_value))
void fcos();          // cos [variable_name] [real_value]; (variable = cos(real_value))
void ftan();          // tan [variable_name] [real_value]; (variable = tan(real_value))
void fasin();         // asin [variable_name] [real_value]; (variable = asin(real_value))
void facos();         // acos [variable_name] [real_value]; (variable = acos(real_value))
void fatan();         // atan [variable_name] [real_value]; (variable = atan(real_value))
void fatan2();        // atan2 [variable_name] [real_value_x] [real_value_y]; variable = atan2(real_value_y, real_value_x);
void fpolk();         // polk [variable_name] [key_code]; returns the state of the key (pressed?true:false) and stores it in the variable_name;
void fjmp();          // jmp [label_name]; unconditional jump to the given label
void fif();           // if [condition]; if the condition is true, this statement is basically ignored and execution continues, otherwise execution jumps to the end command of the if statement.
void inline fend();   // end; marks the end of an if statement, if statements can be nested up to (INT_MAX) times
void fdel();          // del [variable_name]; deletes a variable, if the variable doesn't exist a warning is given, but execution does not halt like an error would
void fwait();         // wait [real_value]; pauses execution for a given number of milliseconds
void fcall();         // call [label_name]; saves the current execution index to the call stack, and jumps to the given label
void fret();          // ret; pops the top execution index from the call stack and assigns it to the execution index, returns from a given function
void ftime();         // time [variable_name]; saves the current time in nanoseconds to the given variable
void fcol();          // col [real_value_x] [real_value_y] [real_value_color]; sets the given pixel at (x, y) to the given color
void fpow();          // pow [variable_name] [real_value_base] [real_value_exponent]; calculates base**exponent and stores it to the given variable
void frand();         // rand [variable_name]; stores the value of rand() to the given variable name
void fsrand();        // srand [real_value]; srand(real_value) seeds the rng, by default the rng is seeded on startup of the vm.
					  // Other Commands:
					  //   int;  interups the execution, this allows the vm to perform a screen refresh and a keyboard input refresh
					  //   kill; kill the execution, this ends vm execution, you should always end your program with this statement.

// uncomment this line for a bunch of command line output such as the current execution argument and various command feedback
// oh and you probably have to enable debug mode for most if it as well
#define CPRINT

					  // print an error message based on the code, this doesn't actually affect the execution
void print_err(byte errorcode, string command = "", string meta = "");

int main(int argc, char **argv) {
	STD_IMPORT_PATH = get_path(argv[0]) + "import\\";

	--argc; ++argv;
	usercode = "";

	// clear key_states array to false
	for(int i = 0; i < ALLEGRO_KEY_MAX; i++) {
		key_states[i] = false;
	}

	// argv[0] is supposed to be the input filename, all other inputs are generally ignored until a future date
	if(argc > 0) {
		// load the contents of argv[0] into memory
		fstream ifile(argv[0]);
		if(ifile.is_open()) {
			string line;
			while(getline(ifile, line)) {
				usercode += line + "\n";
			}
			ifile.close();
		}
		else {
			printf("Could not open file %s\n", argv[0]);
			return 1;
		}
	}

	// the local_import_path is the path of the user input, used for .\* import statements
	local_import_path = get_path(argv[0]);
#if defined(_DEBUG)
	cout << "Local Path: " << local_import_path << endl;
#endif

	if(!al_init()) {
		printf("Could not initialize display\n");
		return 1;
	}

	// process import statements
	handle_imports(usercode);

	// The parsing function returns a vector of tokens because generally speaking we don't know how many tokens exist
	// in a solid string mass. Vector access is relatively slow when compaired to array access though, so to try and
	// speed up execution we store all the tokens in the vector to a token array
	vector<token> _tokens = parse_asm(usercode);
	token_size = _tokens.size();
	tokens = new token[token_size];
	for(uint i = 0; i < token_size; i++) {
		tokens[i] = _tokens[i];
	}

	if(token_size == 0) {
		cout << "Null Input Error\n";
		return 1;
	}

	// the original engine was written in Game Maker Studio and this is mostly for legacy support. in this version of the
	// virtual machine _TARGET_GML is 0 and _TARGET_X86 is 1. The reason this is needed is because there are certain uncompatibilities
	// with the Game Maker Engine and the C++ Engine.
	for(uint i = 0; i < token_size; i++) {
		if(tokens[i].tok_value == "_TARGET_GML") {
			tokens[i].tok_value = "0";
			tokens[i].dvalue = 0.0;
			tokens[i].type = TYPE_NUMBER;
		}
		else if(tokens[i].tok_value == "_TARGET_X86" || tokens[i].tok_value == "_TARGET_x86") {
			tokens[i].tok_value = "1";
			tokens[i].dvalue = 1.0;
			tokens[i].type = TYPE_NUMBER;
		}
	}

	// pull the window width, height and scale from the first three tokens of the input
	if(tokens[0].type == TYPE_NUMBER) {
		width = (int) tokens[0].dvalue;
		height = (int) tokens[1].dvalue;
		scale = (int) tokens[2].dvalue;
	}
	else {
		cout << "Expected width, height, and scale arguments to be present at top of main file\n";
		return 1;
	}

	pixels = new unsigned int[width*height];

	if(!validate_tokens()) {
		delete[] tokens;
		delete[] pixels;
		return 1;
	}
#if defined(_DEBUG)
	cout << token_size << " tokens loaded\n";
#endif
	// this just skips beyond the first three tokens
	tindex = TOKEN_OFFSET;

	// allegro stuff
	main_window = al_create_display(width*scale, height*scale);
	main_eq = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS);

	al_init_primitives_addon();
	al_install_keyboard();

	al_register_event_source(main_eq, al_get_display_event_source(main_window));
	al_register_event_source(main_eq, al_get_timer_event_source(timer));
	al_register_event_source(main_eq, al_get_keyboard_event_source());

	srand(time(NULL));

	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_flip_display();

#if defined(_DEBUG) || defined(CPRINT)
	cout << "Creating Thread\n";
#endif

	thread t(execute);
	t.join();

	al_destroy_event_queue(main_eq);
	al_destroy_timer(timer);
	al_destroy_display(main_window);

	delete[] tokens;
	delete[] pixels;

	return 0;
}

string get_path(string fname) {
	int lpos = fname.find_last_of('\\');
	if(lpos == string::npos) {
		lpos = fname.find_last_of("/");
		if(lpos == string::npos) {
			cout << "Error retrieveing path of " << fname << endl;
		}
	}
	return fname.substr(0, lpos + (lpos != string::npos ? 1 : 0));
}
byte validate_command_noarg(uint start_index) {
	if(tokens[start_index] == ";") {
		return ERROR_NOERROR;
	}
	return ERROR_SEMICOLON;
}
byte validate_command_1arg(uint start_index, byte argument_type) {
	if(tokens[start_index + 1] == ";") {
		bool valid_argument = false;
		if(argument_type & ARGUMENT_NUMBER) {
			if(tokens[start_index].type == TYPE_NUMBER) {
				valid_argument = true;
			}
		}
		if(argument_type & ARGUMENT_DIRECTIVE) {
			if(tokens[start_index].type == TYPE_DIRECTIVE) {
				valid_argument = true;
			}
		}
		if(argument_type & ARGUMENT_VARIABLE) {
			if(tokens[start_index].type == TYPE_STRING) {
				valid_argument = true;
			}
		}
		if(valid_argument) {
			return ERROR_NOERROR;
		}
		return ERROR_INVALID_ARGUMENT;
	}
	else {
		return ERROR_SEMICOLON;
	}
}
byte validate_command_2arg(uint start_index, byte argument_type1, byte argument_type2) {
	if(tokens[start_index + 2] == ";") {
		bool valid_argument = false;
		if(argument_type1 & ARGUMENT_NUMBER) {
			if(tokens[start_index].type == TYPE_NUMBER) {
				valid_argument = true;
			}
		}
		if(argument_type1 & ARGUMENT_DIRECTIVE) {
			if(tokens[start_index].type == TYPE_DIRECTIVE) {
				valid_argument = true;
			}
		}
		if(argument_type1 & ARGUMENT_VARIABLE) {
			if(tokens[start_index].type == TYPE_STRING) {
				valid_argument = true;
			}
		}
		if(!valid_argument) {
			return ERROR_INVALID_ARGUMENT;
		}

		if(argument_type2 & ARGUMENT_NUMBER) {
			if(tokens[start_index + 1].type == TYPE_NUMBER) {
				valid_argument = true;
			}
		}
		if(argument_type2 & ARGUMENT_DIRECTIVE) {
			if(tokens[start_index + 1].type == TYPE_DIRECTIVE) {
				valid_argument = true;
			}
		}
		if(argument_type2 & ARGUMENT_VARIABLE) {
			if(tokens[start_index + 1].type == TYPE_STRING) {
				valid_argument = true;
			}
		}
		if(!valid_argument) {
			return ERROR_INVALID_ARGUMENT;
		}
		return ERROR_NOERROR;
	}
	else {
		return ERROR_SEMICOLON;
	}
}
byte validate_command_3arg(uint start_index, byte argument_type1, byte argument_type2, byte argument_type3) {
	if(tokens[start_index + 3] == ";") {
		bool valid_argument = false;
		if(argument_type1 & ARGUMENT_NUMBER) {
			if(tokens[start_index].type == TYPE_NUMBER) {
				valid_argument = true;
			}
		}
		if(argument_type1 & ARGUMENT_DIRECTIVE) {
			if(tokens[start_index].type == TYPE_DIRECTIVE) {
				valid_argument = true;
			}
		}
		if(argument_type1 & ARGUMENT_VARIABLE) {
			if(tokens[start_index].type == TYPE_STRING) {
				valid_argument = true;
			}
		}
		if(!valid_argument) {
			return ERROR_INVALID_ARGUMENT;
		}

		if(argument_type2 & ARGUMENT_NUMBER) {
			if(tokens[start_index + 1].type == TYPE_NUMBER) {
				valid_argument = true;
			}
		}
		if(argument_type2 & ARGUMENT_DIRECTIVE) {
			if(tokens[start_index + 1].type == TYPE_DIRECTIVE) {
				valid_argument = true;
			}
		}
		if(argument_type2 & ARGUMENT_VARIABLE) {
			if(tokens[start_index + 1].type == TYPE_STRING) {
				valid_argument = true;
			}
		}
		if(!valid_argument) {
			return ERROR_INVALID_ARGUMENT;
		}

		if(argument_type3 & ARGUMENT_NUMBER) {
			if(tokens[start_index + 1].type == TYPE_NUMBER) {
				valid_argument = true;
			}
		}
		if(argument_type3 & ARGUMENT_DIRECTIVE) {
			if(tokens[start_index + 1].type == TYPE_DIRECTIVE) {
				valid_argument = true;
			}
		}
		if(argument_type3 & ARGUMENT_VARIABLE) {
			if(tokens[start_index + 1].type == TYPE_STRING) {
				valid_argument = true;
			}
		}
		if(!valid_argument) {
			return ERROR_INVALID_ARGUMENT;
		}
		return ERROR_NOERROR;
	}
	else {
		return ERROR_SEMICOLON;
	}
}
bool validate_tokens() {
	for(uint i = TOKEN_OFFSET; i < token_size; i++) {
		token t = tokens[i];
		if(t == "ret" || t == "end" || t == "flush" || t == "kill" || t == "int" || t == "inline") {
			byte err = validate_command_noarg(i + 1);
			if(err == ERROR_NOERROR) {
				assign_cmd_value(tokens[i]);
				i++;
			}
			else {
				print_err(err, t.tok_value, "");
				return false;
			}
		}
		else if(t == "def" || t == "inc" || t == "dec" || t == "del" || t == "time" || t == "rand") {
			// single argument variable only
			byte err = validate_command_1arg(i + 1, ARGUMENT_VARIABLE);
			if(err == ERROR_NOERROR) {
				assign_cmd_value(tokens[i]);
				i += 2;
			}
			else {
				print_err(err, t.tok_value, "");
				return false;
			}
		}
		else if(t == "jmp" || t == "call") {
			// single argument directive only
			byte err = validate_command_1arg(i + 1, ARGUMENT_DIRECTIVE);
			if(err == ERROR_NOERROR) {
				assign_cmd_value(tokens[i]);
				i += 2;
			}
			else {
				print_err(err, t.tok_value, "");
				return false;
			}
		}
		else if(t == "if" || t == "wait" || t == "srand" || t == "print" || t == "putc") {
			// single argument variable|number
			byte err = validate_command_1arg(i + 1, ARGUMENT_VARIABLE | ARGUMENT_NUMBER);
			if(err == ERROR_NOERROR) {
				assign_cmd_value(tokens[i]);
				uint s = i;
				i += 2;

				// find jumpto position
				if(t == "if") {
					// searches for the paired end statement taking into account nested if statements
					int icount = 1;
					for(uint j = i; j < token_size; j++) {
						icount += tokens[j] == "if";
						icount -= tokens[j] == "end";

						// we wait until the semicolon after the final end command primarily to save on the guesswork when it comes to the ACTUAL
						// next command index
						if(tokens[i] == ";" && !icount) {
#if defined(_DEBUG) && defined(CPRINT)
							cout << "Assigning jump value " << j << " to token " << tokens[s].tok_value << " at position " << i << "/" << token_size << endl;
							cout << "\tJumpto is " << tokens[j].tok_value << endl;
#endif
							tokens[s].jumpto = j;
							break;
						}
					}
					if(icount > 0) {
						print_err(ERROR_OTHER, "if", "Could not find paired 'end' command for statement");
						return false;
					}
				}

			}
			else {
				print_err(err, t.tok_value, "");
				return false;
			}
		}
		else if(t == "not" || t == "bnot" || t == "ld" || t == "sin" || t == "cos" || t == "tan" ||
			t == "acos" || t == "asin" || t == "atan" || t == "sqrt" || t == "polk" || t == "const") {
			// 2 argument (variable, variable|number)
			byte err = validate_command_2arg(i + 1, ARGUMENT_VARIABLE, ARGUMENT_VARIABLE | ARGUMENT_NUMBER);
			if(err == ERROR_NOERROR) {
				assign_cmd_value(tokens[i]);
				i += 3;
			}
			else {
				print_err(err, t.tok_value, "");
				return false;
			}
		}
		else if(t == "add" || t == "sub" || t == "mul" || t == "div" || t == "mod" || t == "gth" || t == "lth" || t == "eq" ||
			t == "neq" || t == "lshift" || t == "rshift" || t == "or" || t == "and" || t == "xor" || t == "bor" ||
			t == "band" || t == "bxor" || t == "atan2" || t == "pow") {
			// 3 argument (variable, variable|number, variable|number)
			byte err = validate_command_3arg(i + 1, ARGUMENT_VARIABLE, ARGUMENT_VARIABLE | ARGUMENT_NUMBER, ARGUMENT_VARIABLE | ARGUMENT_NUMBER);
			if(err == ERROR_NOERROR) {
				assign_cmd_value(tokens[i]);
				i += 4;
			}
			else {
				print_err(err, t.tok_value, "");
				return false;
			}
		}
		else if(t == "col") {
			// 3 argument (real, real, real)
			byte err = validate_command_3arg(i + 1, ARGUMENT_VARIABLE | ARGUMENT_NUMBER, ARGUMENT_VARIABLE | ARGUMENT_NUMBER, ARGUMENT_VARIABLE | ARGUMENT_NUMBER);
			if(err == ERROR_NOERROR) {
				assign_cmd_value(tokens[i]);
				i += 4;
			}
			else {
				print_err(err, t.tok_value, "");
			}
		}
		else {
			// this somewhate complex looking condition simply prevents jump indexing to command arguments
			// if the token is a directive and we ASSUME it's the correct label, we could find ourselves jumping
			// to "jmp .loopstart" statements instead of the actual .loopstart label. Basically we assume
			// it's a label declaration if the type is that of a label AND either the previous token is less
			// that the token offset (anything less than the token offset is meta data such as window size) or
			// the previous token is a semicolon, which marks the end of the previous command.
			if(t.type == TYPE_DIRECTIVE && (i - 1 < TOKEN_OFFSET || tokens[i - 1] == ";")) {
				for(uint j = i; j < token_size; j++) {
					if(tokens[j] == ";") {
						label_op[t.tok_value] = j + 1;
						i = j;
						break;
					}
				}
			}
			else if(t.type == TYPE_STRING && t.tok_value == ";") {
				// empty statement allowed
			}
			else {
				print_err(ERROR_INVALID_COMMAND, t.tok_value, "");
#if defined(_DEBUG)
				cout << "Token feed: \n";
				for(uint j = max(TOKEN_OFFSET, i - 12); j < min(i + 12, token_size); j++) {
					cout << " " << tokens[j].tok_value + " ";
				}
#endif
				return false;
			}
		}
	}
	return true;
}
long inline current_time() {
	return duration_cast<nanoseconds> (
		system_clock::now().time_since_epoch()
		).count();
}
long inline current_time_ms() {
	return duration_cast<milliseconds> (
		system_clock::now().time_since_epoch()
		).count();
}
variable get_variable(string vname) {
	variable v = variables.find(vname);
	if(v == variables.end()) {
		variable_access_flag = true;
	}
	return v;
}
void find_jump_points() {
	// depreciated
	for(uint i = 0; i < token_size; i++) {
		if(tokens[i].type == TYPE_DIRECTIVE) {
			if(i - 1 < 3 || tokens[i - 1] == ";") {
				for(uint j = i; j < token_size; j++) {
					if(tokens[j] == ";") {
						label_op[tokens[i].tok_value] = j + 1;
						break;
					}
				}
			}
		}
	}
}
void handle_imports(string &feed) {
	int pos;
#if defined(_DEBUG)
	int mcount = 0;
#endif
	while((pos = feed.find(".import", 0)) != string::npos) {
		string fname = "", ofname;
		for(uint i = pos + 8; i < feed.length(); i++) {
			char c = feed[i];
			if(c == ';') {
				break;
			}
			fname += c;
		}
		ofname = fname;
		bool s = 0; // normal

		if(fname.substr(0, min(fname.length(), 4)) == "std:") {
			fname.replace(0, 4, STD_IMPORT_PATH);
			fname += ".a";
			s = 1; // std;
		}
		else if(fname.substr(0, min(fname.length(), 2)) == ".\\" || fname.substr(0, min(fname.length(), 2)) == "./") {
			fname.replace(0, 2, local_import_path);
			s = 1; // local;
		}

		cout << "fetching contents of " << fname << endl;

		string content = "";
		fstream ifile(fname);
		if(ifile.is_open()) {
			string line;
			while(getline(ifile, line)) {
				content += line + "\n";
			}
			ifile.close();
		}
		else {
			cout << "Could not import " << fname << "\n";
		}
#if defined(_DEBUG)
		int count = 0, p = 0;
		while((p = content.find(".import", p) != string::npos)) {
			count++;
			p += 7;
		}
		cout << "Replacing " << feed.substr(pos, 8 + (s == 0 ? fname.length() : ofname.length())) << endl;
		cout << "with string " << content.length() << " characters long and " << count << " imports\n\n";

		mcount++;
#endif
		feed.replace(pos, 8 + (s == 0 ? fname.length() : ofname.length()), content);
	}
#if defined(_DEBUG)
	cout << "Imported " << mcount << " modules\n";
#endif
}
void render() {
#if defined(_DEBUG) || defined(CPRINT)
	cout << "Execute Render...\n";
#endif
	for(int i = 0; i < width; i++) {
#if defined(_DEBUG) || defined(CPRINT)
		cout << "\tColumn " << i << "/" << width << endl;
#endif
		for(int j = 0; j < height; j++) {
#if defined(_DEBUG) || defined(CPRINT)
			cout << "\tRow " << j << "/" << height << endl;
#endif
			int r, g, b; // colors are saved in a single unsigned integer, rrggbb format
			r = ((pixels[INDEX(i, j, width)]) >> 16) & 0b11111111;
			g = ((pixels[INDEX(i, j, width)]) >> 8) & 0b11111111;
			b = pixels[INDEX(i, j, width)] & 0b11111111;
#if defined(_DEBUG) || defined(CPRINT)
			cout << "\tCol(" << r << "," << g << "," << b << ")\n";
#endif
			al_draw_filled_rectangle(i*scale, j*scale, (i + 1)*scale, (j + 1)*scale, al_map_rgb(r, g, b));
		}
	}
#if defined(_DEBUG) || defined(CPRINT)
	cout << "Done Drawing Pixels\n";
#endif
	al_flip_display();
}

void fdef() {
	token name = tokens[tindex++];
	variables[name.tok_value] = 0.0;
	tindex++;
}
void finc() {
	token name = tokens[tindex++];
	variable v = get_variable(name.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << name.tok_value << " for 'inc'" << endl;
		THROW_ERROR();
	}
	v->second += 1.0;
	tindex++;
}
void fdec() {
	token name = tokens[tindex++];
	variable v = get_variable(name.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << name.tok_value << " for 'inc'" << endl;
		THROW_ERROR();
	}
	v->second -= 1.0;
	tindex++;
}
void fconst() {
	uint cmd_index = tindex - 1;
	token name = tokens[tindex++], value = tokens[tindex++];
	constants[name.tok_value] = value;
	tokens[cmd_index].tok_value = ";";
	tokens[cmd_index + 1].tok_value = ";";
	tokens[cmd_index + 2].tok_value = ";";
	for(uint i = TOKEN_OFFSET; i < token_size; i++) {
		if(tokens[i] == name.tok_value) {
			tokens[i] = value;
		}
	}
	tindex++;
}
void fld() {
	token name = tokens[tindex++], value = tokens[tindex++];
	variable n = get_variable(name.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << name.tok_value << " for 'ld'" << endl;
		THROW_ERROR();
	}
	double v;
	if(value.type == TYPE_STRING) {
		variable a = get_variable(value.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << name.tok_value << " for 'ld'" << endl;
			THROW_ERROR();
		}
		v = a->second;
	}
	else {
		v = value.dvalue;
	}
	n->second = v;
	tindex++;
}
void fadd() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'add' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'add' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'add' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = lhv + rhv;
	tindex++;
}
void fsub() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'sub' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'sub' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'sub' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = lhv - rhv;
	tindex++;
}
void feq() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'eq' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'eq' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'eq' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = lhv == rhv;
	tindex++;
}
void fneq() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'neq' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'neq' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'neq' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = lhv != rhv;
	tindex++;
}
void fprint() {
	token val = tokens[tindex++];
	if(val.type == TYPE_NUMBER) {
		cout << val.dvalue << endl;
		tindex++;
		return;
	}
	variable h = get_variable(val.tok_value);
	if(variable_access_flag) {
		cout << "Unknown variable " << val.tok_value << " for command 'print'\n";
		THROW_ERROR();
	}
	cout << h->second;
	tindex++;
}
void fputc() {
	token val = tokens[tindex++];
	if(val.type == TYPE_NUMBER) {
		cout << (char) val.dvalue;
		tindex++;
		return;
	}
	variable h = get_variable(val.tok_value);
	if(variable_access_flag) {
		cout << "Unknown variable " << val.tok_value << " for command 'putc'\n";
		THROW_ERROR();
	}
	cout << (char) h->second;
	tindex++;
}
void inline fflush() {
	tindex++;
}
void flth() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'lth' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'lth' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'lth' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = (double) (lhv < rhv);
	tindex++;
}
void fgth() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'gth' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'gth' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'gth' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = (double) (lhv > rhv);
	tindex++;
}
void fmul() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'mul' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'mul' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'mul' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = lhv * rhv;
	tindex++;
}
void fdiv() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'div' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'div' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'div' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = lhv / rhv;
	tindex++;
}
void fmod() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'mod' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'mod' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'mod' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = (double) ((long) lhv % (long) rhv);
	tindex++;
}
void flshift() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'lshift' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'lshift' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'lshift' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = (double) ((long) lhv << (long) rhv);
	tindex++;
}
void frshift() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'rshift' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'rshift' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'rshift' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = (double) ((long) lhv >> (long) rhv);
	tindex++;
}
void flor() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'or' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'or' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'or' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = lhv || rhv;
	tindex++;
}
void fland() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'and' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'and' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'and' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = lhv && rhv;
	tindex++;
}
void flxor() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'xor' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'xor' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'xor' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = !((lhv && rhv) || (!lhv && !rhv));
	tindex++;
}
void fband() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'band' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'band' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'band' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = (double) ((long) lhv & (long) rhv);
	tindex++;
}
void fbor() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'bor' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'bor' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'bor' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = (double) ((long) lhv | (long) rhv);
	tindex++;
}
void fbxor() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'xor' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'xor' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'xor' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = (double) ((long) lhv ^ (long) rhv);
	tindex++;
}
void flnot() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'not' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'not' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	varhandle->second = (double) !dval;
	tindex++;
}
void fbnot() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'bnot' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'bnot' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	varhandle->second = (double) ~(long) dval;
	tindex++;
}
void fsqrt() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'sqrt' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'sqrt' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	varhandle->second = sqrt(dval);
	tindex++;
}
void fsin() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'sin' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'sin' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	varhandle->second = sin(dval);
	tindex++;
}
void fcos() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'cos' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'cos' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	varhandle->second = cos(dval);
	tindex++;
}
void ftan() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'tan' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'tan' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	varhandle->second = tan(dval);
	tindex++;
}
void fasin() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'asin' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'asin' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	varhandle->second = asin(dval);
	tindex++;
}
void facos() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'acos' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'acos' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	varhandle->second = acos(dval);
	tindex++;
}
void fatan() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'atan' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'atan' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	varhandle->second = atan(dval);
	tindex++;
}
void fatan2() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];

	variable varhandle = get_variable(var.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << var.tok_value << "for 'atan2' command\n";
		THROW_ERROR();
	}

	double lhv, rhv;
	if(lh.type == TYPE_NUMBER) {
		lhv = lh.dvalue;
	}
	else {
		variable v = get_variable(lh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << lh.tok_value << "for 'atan2' command\n";
			THROW_ERROR();
		}
		lhv = v->second;
	}

	if(rh.type == TYPE_NUMBER) {
		rhv = rh.dvalue;
	}
	else {
		variable v = get_variable(rh.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << rh.tok_value << "for 'atan2' command\n";
			THROW_ERROR();
		}
		rhv = v->second;
	}

	varhandle->second = atan2(rhv, lhv);
	tindex++;
}
void fpolk() {
	token var = tokens[tindex++], val = tokens[tindex++];
	variable varhandle = get_variable(var.tok_value);
	double dval;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'polk' command\n";
		THROW_ERROR();
	}

	if(val.type == TYPE_STRING) {
		variable vhandle = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << "for 'polk' command\n";
			THROW_ERROR();
		}
	}
	else {
		dval = val.dvalue;
	}

	if(dval < 0 || dval >= ALLEGRO_KEY_MAX) {
		cout << "! Keyboard access out of bounds " << dval << endl;
		varhandle->second = false;
		tindex++;
		return;
	}

	varhandle->second = (double) key_states[(long) dval];
	tindex++;
}
void fjmp() {
	token directive = tokens[tindex++];
	label l = label_op.find(directive.tok_value);
	if(l == label_op.end()) {
		cout << "Attempted jump to an unknown label: " << directive.tok_value << endl;
		THROW_ERROR();
	}
	tindex = (uint) l->second;
}
void fif() {
	token *iftok = &tokens[tindex - 1], *val = &tokens[tindex++];
	double condition;
	if(val->type == TYPE_STRING) {
		variable v = get_variable(val->tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val->tok_value << " for 'if' command\n";
			THROW_ERROR();
		}
		condition = v->second;
	}
	else {
		condition = val->dvalue;
	}

	if(condition) {
		tindex++;
	}
	else {
		tindex = iftok->jumpto;
	}
}
void inline fend() {
	tindex++;
}
void fdel() {
	variable v = get_variable(tokens[tindex++].tok_value);
	if(variable_access_flag) {
		cout << "! Trying to delete non-existant variable " << v->first << endl;
		tindex++;
		return;
	}
	variables.erase(v);
	tindex++;
}
void fwait() {
	token val = tokens[tindex++];
	long pause;
	if(val.type == TYPE_STRING) {
		variable h = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << val.tok_value << " for 'wait' command\n";
			THROW_ERROR();
		}
		pause = (long) h->second;
	}
	else {
		pause = (long) val.dvalue;
	}

	Sleep(pause);

	tindex++;
}
void fcall() {
	token directive = tokens[tindex++];
	label l = label_op.find(directive.tok_value);
	if(l == label_op.end()) {
		cout << "Attempted call to an unknown label: " << directive.tok_value << endl;
		THROW_ERROR();
	}
	call_stack.push(tindex + 1);
	tindex = (uint) l->second;
}
void fret() {
	if(call_stack.size() < 1) {
		cout << "Unexpected 'ret' command\n";
		THROW_ERROR();
	}

	tindex = call_stack.top();
	call_stack.pop();
}
void ftime() {
	token name = tokens[tindex++];
	variable v = get_variable(name.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << name.tok_value << " for 'time'" << endl;
		THROW_ERROR();
	}
	v->second = (double) current_time();
	tindex++;
}
void fcol() {
	token xtok = tokens[tindex++], ytok = tokens[tindex++], coltok = tokens[tindex++];
	long xpos, ypos;
	uint color;

	if(xtok.type == TYPE_NUMBER) {
		xpos = (long) xtok.dvalue;
	}
	else {
		variable v = get_variable(xtok.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << xtok.tok_value << " for 'col' command\n";
			THROW_ERROR();
		}
		xpos = (long) v->second;
	}

	if(ytok.type == TYPE_NUMBER) {
		ypos = (long) ytok.dvalue;
	}
	else {
		variable v = get_variable(ytok.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << ytok.tok_value << " for 'col' command\n";
			THROW_ERROR();
		}
		ypos = (long) v->second;
	}

	if(coltok.type == TYPE_NUMBER) {
		color = (uint) coltok.dvalue;
	}
	else {
		variable v = get_variable(coltok.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << coltok.tok_value << " for 'col' command\n";
			THROW_ERROR();
		}
		color = (uint) v->second;
	}

	uint index = INDEX(xpos, ypos, width);
	if(0 <= index && index < (width*height)) {
		pixels[index] = color;
	}
	tindex++;
}
void fpow() {
	token var = tokens[tindex++], num = tokens[tindex++], exp = tokens[tindex++];
	variable v = get_variable(var.tok_value);
	double low, high;

	if(variable_access_flag) {
		cout << "Unknown variable " << var.tok_value << " for 'pow' command\n";
		THROW_ERROR();
	}

	if(num.type == TYPE_NUMBER) {
		low = num.dvalue;
	}
	else {
		variable h = get_variable(num.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << var.tok_value << " for 'pow' command\n";
			THROW_ERROR();
		}
		low = h->second;
	}

	if(exp.type == TYPE_NUMBER) {
		high = exp.dvalue;
	}
	else {
		variable h = get_variable(exp.tok_value);
		if(variable_access_flag) {
			cout << "Unknown variable " << var.tok_value << " for 'pow' command\n";
			THROW_ERROR();
		}
		high = h->second;
	}

	v->second = pow(low, high);
}
void frand() {
	token name = tokens[tindex++];
	variable v = get_variable(name.tok_value);
	if(variable_access_flag) {
		cout << "Undefined variable " << name.tok_value << " for 'rand'\n";
		THROW_ERROR();
	}
	v->second = (double) rand();
	tindex++;
}
void fsrand() {
	token val = tokens[tindex++];
	if(val.type == TYPE_NUMBER) {
		srand((unsigned long) val.dvalue);
	}
	else {
		variable v = get_variable(val.tok_value);
		if(variable_access_flag) {
			cout << "Undefined variable " << val.tok_value << " for 'srand'\n";
		}
		srand((long) v->second);
	}
	tindex++;
}

void print_err(byte errorcode, string command, string meta) {
	switch(errorcode) {
	case ERROR_INVALID_ARGUMENT: {
		cout << "Error, invalid argument for command: " << command << "\nInfo: " << meta << endl;
		break;
	}
	case ERROR_INVALID_COMMAND: {
		cout << "Error, unknown command: " << command << "\nInfo: " << meta << endl;
		break;
	}
	case ERROR_SEMICOLON: {
		cout << "Error, expected semicolon (;) after command: " << command << "\nInfo: " << meta << endl;
		break;
	}
	case ERROR_UNKNOWN_VARIABLE: {
		cout << "Error, unknown variable in command: " << command << "\nInfo: " << meta << endl;
		break;
	}
	case ERROR_UNKNOWN_DIRECTIVE: {
		cout << "Error, unknown directive used in command: " << command << "\nInfo: " << meta << endl;
		break;
	}
	case ERROR_OTHER: {
		cout << "Error in command: " << command << "\nInfo: " << meta << endl;
		break;
	}
	}
}

void execute() {
#if defined(_DEBUG) || defined(CPRINT)
	cout << "Begining Process\n";
#endif
	long icount = 0, stime = current_time_ms();

	al_start_timer(timer);
	while(run_loop) {
#if defined(_DEBUG) || defined(CPRINT)
		cout << "Main Loop\n";
#endif

		ALLEGRO_EVENT ev;
		al_wait_for_event(main_eq, &ev);
#if defined(_DEBUG) || defined(CPRINT)
		cout << "Got Event\n";
#endif
		switch(ev.type) {
		case ALLEGRO_EVENT_TIMER: { // in theory this is called every 1/FPS seconds
#if defined(_DEBUG) || defined(CPRINT)
			cout << "Timer Event\n";
#endif
			render();
#if defined(_DEBUG) || defined(CPRINT)
			cout << "Finished Render\n";
#endif
			if(suspend) break;

			// keep executing statements until exec is set to false, this happens in one of three situations:
			// 1: int command is called
			// 2: kill command is called
			// 3: An error occurs

			exec = true;
			long _time = current_time();
			while(exec) {
				if(tindex >= token_size) {
					cout << "Unexpected end of token feed\n";
					exec = false;
					run_loop = false;
					break;
				}

				token tok = tokens[tindex++];
#if defined(_DEBUG) || defined(CPRINT)
				cout << "Executing " << tok.tok_value << " at " << tindex << "/" << token_size << endl;
#endif
				icount++;
				// execute the corresponding function to the command given using the command code instead of the string representation
				// of the command
				switch(tok.cmdval) {
				case Commands::def: {
					fdef();
					break;
				}
				case Commands::inc: {
					finc();
					break;
				}
				case Commands::dec: {
					fdec();
					break;
				}
				case Commands::cnst: {
					fconst();
					break;
				}
				case Commands::ld: {
					fld();
					break;
				}
				case Commands::add: {
					fadd();
					break;
				}
				case Commands::sub: {
					fsub();
					break;
				}
				case Commands::eq: {
					feq();
					break;
				}
				case Commands::neq: {
					fneq();
					break;
				}
				case Commands::print: {
					fprint();
					break;
				}
				case Commands::putch: {
					fputc();
					break;
				}
				case Commands::flush: {
					fflush();
					break;
				}
				case Commands::lth: {
					flth();
					break;
				}
				case Commands::gth: {
					fgth();
					break;
				}
				case Commands::mul: {
					fmul();
					break;
				}
				case Commands::divd: {
					fdiv();
					break;
				}
				case Commands::mod: {
					fmod();
					break;
				}
				case Commands::lshift: {
					flshift();
					break;
				}
				case Commands::rshift: {
					frshift();
					break;
				}
				case Commands::lor: {
					flor();
					break;
				}
				case Commands::land: {
					fland();
					break;
				}
				case Commands::lxor: {
					flxor();
					break;
				}
				case Commands::band: {
					fband();
					break;
				}
				case Commands::bor: {
					fbor();
					break;
				}
				case Commands::bxor: {
					fbxor();
					break;
				}
				case Commands::lnot: {
					flnot();
					break;
				}
				case Commands::bnot: {
					fbnot();
					break;
				}
				case Commands::squrt: {
					fsqrt();
					break;
				}
				case Commands::dsin: {
					fsin();
					break;
				}
				case Commands::dcos: {
					fcos();
					break;
				}
				case Commands::dtan: {
					ftan();
					break;
				}
				case Commands::dasin: {
					fasin();
					break;
				}
				case Commands::dacos: {
					facos();
					break;
				}
				case Commands::datan: {
					fatan();
					break;
				}
				case Commands::datan2: {
					fatan2();
					break;
				}
				case Commands::polk: {
					fpolk();
					break;
				}
				case Commands::jmp: {
					fjmp();
					break;
				}
				case Commands::fi: {
					fif();
					break;
				}
				case Commands::end: {
					fend();
					break;
				}
				case Commands::del: {
					fdel();
					break;
				}
				case Commands::wait: {
					fwait();
					break;
				}
				case Commands::call: {
					fcall();
					break;
				}
				case Commands::ret: {
					fret();
					break;
				}
				case Commands::dtime: {
					ftime();
					break;
				}
				case Commands::col: {
					fcol();
					break;
				}
				case Commands::dpow: {
					fpow();
					break;
				}
				case Commands::drand: {
					frand();
					break;
				}
				case Commands::dsrand: {
					fsrand();
					break;
				}
				case Commands::intr: {
					exec = false;
					tindex++;
					break;
				}
				case Commands::kill: {
					exec = false;
					run_loop = false;
					break;
				}
				default: { // because validate_tokens has been called, if we reach this point, we can generally assume we have either encountered a label or a semicolon
					break;
				}
				}
#if defined(_DEBUG) && !defined(CPRINT)
				// delay between calling commands, only in debug mode and even in debug mode, delay is set to zero by default
				Sleep(delay / 1000000);
#endif
			}
			break;
		}
		case ALLEGRO_EVENT_KEY_DOWN: {
#if defined(_DEBUG) || defined(CPRINT)
			cout << "Down Key Event\n";
#endif
			key_states[ev.keyboard.keycode] = true;
			if(ev.keyboard.keycode == ALLEGRO_KEY_F3) {
				suspend = !suspend;
			}
			else if(ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
				run_loop = false;
			}
			break;
		}
		case ALLEGRO_EVENT_KEY_UP: {
#if defined(_DEBUG) || defined(CPRINT)
			cout << "Up Key Event\n";
#endif
			key_states[ev.keyboard.keycode] = false;
			break;
		}
		case ALLEGRO_EVENT_DISPLAY_CLOSE: {
#if defined(_DEBUG) || defined(CPRINT)
			cout << "Close Display Event\n";
#endif
			run_loop = false;
			break;
		}
#if defined(_DEBUG) || defined(CPRINT)
		default: {
			cout << "Other Event\n";
			break;
		}
#endif
		}
	}
	al_stop_timer(timer);

#if defined(_DEBUG)
	variable v = variables.begin();
	for(; v != variables.end(); v++) {
		cout << v->first << ": " << v->second << endl;
	}
#endif

	long etime = current_time_ms();
	cout << icount << " operations in " << etime - stime << "(ms) -> " << (double) (icount / ((etime - stime) / 1000.)) << " ops\n";
}