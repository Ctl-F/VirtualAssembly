#pragma once

#include <string>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <iostream>

using std::vector;
using std::string;
using std::atof;
using std::cout;
using std::endl;
using std::stringstream;
using std::hex;

#define TYPE_STRING 0
#define TYPE_NUMBER 1
#define TYPE_DIRECTIVE 2
#define TYPE_NONE 3

#define IS_NUMERIC(a) (48 <= a && a < 58)
#define MIN(a, b) (a<b?a:b)

struct token{
	unsigned char type = TYPE_NONE; // type of token
	bool is_array = false; // is the token an array? (this hasn't been implemented, and may not ever be)
	string tok_value = ""; // the actual string representation of the token
	double dvalue; // the value of the token if is of TYPE_NUMBER
	unsigned char cmdval = 255; // the integer command value of the token, see command.h for more details
	unsigned int jumpto = -1; // this is for use with if, jmp, and call, to make it so we don't have to iterate nor look up the place to jump to, this has only been implemented in the if statement though
	bool s_lit = false;

	string array_index; // unimplemented, may never be implemented
	unsigned char array_index_type; // unimplemented

	bool operator==(string s) { // more convenient token == syntax rather than token.tok_value == s
		return tok_value == s;
	}
	bool operator==(char *s) {
		return tok_value == s;
	}
	bool operator==(const char *s) {
		return tok_value == s;
	}
	bool operator==(unsigned char code) { // check against cmd value
		return cmdval == code;
	}
	bool operator==(int code) { // check against cmd value
		return cmdval == code;
	}
};

// this is the blanket function call. The other functions are specialized to parse their respective tokens from
// the begining of the string using the longest token rule
vector<token> parse_asm(string feed); 

// use the longest token rule to parse their respective tokens from the begining of the string
token parse_directive(string &feed);
token parse_identifier(string &feed);
token parse_number(string &feed);
token parse_hex(string &feed);
token parse_string(string &feed);
bool parse_whitespace(string &feed);
bool parse_comment(string &feed);