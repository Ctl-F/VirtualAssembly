#pragma once

#include "parsing.h"

// the command values, we do this because token.tok_value == "def" is much slower than token.cmdval == 0;
// and while under ordinary circumstances this may not be a huge difference, but over many iterations, this
// can be significant
enum Commands : unsigned char {
	def,
	inc,
	dec,
	cnst,
	ld,
	add,
	sub,
	eq,
	neq,
	print,
	putch,
	flush,
	lth,
	gth,
	mul,
	divd,
	mod,
	lshift,
	rshift,
	lor,
	land,
	lxor,
	band,
	bor,
	bxor,
	lnot,
	bnot,
	squrt,
	dsin,
	dcos,
	dtan,
	dasin,
	dacos,
	datan,
	datan2,
	polk,
	jmp,
	fi,
	end,
	del,
	wait,
	call,
	ret,
	dtime,
	col,
	dpow,
	drand,
	dsrand,
	intr,
	kill,
	unknown,
	ldimg,
	img,
	frimg,
	aalloc,
	afree,
	aget,
	aset,
	alen
};

// do the string comparison now, perform integer comparison later
void assign_cmd_value(token &t) {
	if(t == "def") {
		t.cmdval = Commands::def;
	}
	else if(t == "inc") {
		t.cmdval = Commands::inc;
	}
	else if(t == "dec") {
		t.cmdval = Commands::dec;
	}
	else if(t == "const") {
		t.cmdval = Commands::cnst;
	}
	else if(t == "ld") {
		t.cmdval = Commands::ld;
	}
	else if(t == "add") {
		t.cmdval = Commands::add;
	}
	else if(t == "sub") {
		t.cmdval = Commands::sub;
	}
	else if(t == "eq") {
		t.cmdval = Commands::eq;
	}
	else if(t == "neq") {
		t.cmdval = Commands::neq;
	}
	else if(t == "print") {
		t.cmdval = Commands::print;
	}
	else if(t == "putc") {
		t.cmdval = Commands::putch;
	}
	else if(t == "flush") {
		t.cmdval = Commands::flush;
	}
	else if(t == "lth") {
		t.cmdval = Commands::lth;
	}
	else if(t == "gth") {
		t.cmdval = Commands::gth;
	}
	else if(t == "mul") {
		t.cmdval = Commands::mul;
	}
	else if(t == "div") {
		t.cmdval = Commands::divd;
	}
	else if(t == "mod") {
		t.cmdval = Commands::mod;
	}
	else if(t == "lshift") {
		t.cmdval = Commands::lshift;
	}
	else if(t == "rshift") {
		t.cmdval = Commands::rshift;
	}
	else if(t == "or") {
		t.cmdval = Commands::lor;
	}
	else if(t == "and") {
		t.cmdval = Commands::land;
	}
	else if(t == "xor") {
		t.cmdval = Commands::lxor;
	}
	else if(t == "band") {
		t.cmdval = Commands::band;
	}
	else if(t == "bor") {
		t.cmdval = Commands::bor;
	}
	else if(t == "bxor") {
		t.cmdval = Commands::bxor;
	}
	else if(t == "not") {
		t.cmdval = Commands::lnot;
	}
	else if(t == "bnot") {
		t.cmdval = Commands::bnot;
	}
	else if(t == "sqrt") {
		t.cmdval = Commands::squrt;
	}
	else if(t == "sin") {
		t.cmdval = Commands::dsin;
	}
	else if(t == "cos") {
		t.cmdval = Commands::dcos;
	}
	else if(t == "tan") {
		t.cmdval = Commands::dtan;
	}
	else if(t == "asin") {
		t.cmdval = Commands::dasin;
	}
	else if(t == "acos") {
		t.cmdval = Commands::dacos;
	}
	else if(t == "atan") {
		t.cmdval = Commands::datan;
	}
	else if(t == "atan2") {
		t.cmdval = Commands::datan2;
	}
	else if(t == "polk") {
		t.cmdval = Commands::polk;
	}
	else if(t == "jmp") {
		t.cmdval = Commands::jmp;
	}
	else if(t == "if") {
		t.cmdval = Commands::fi;
	}
	else if(t == "end") {
		t.cmdval = Commands::end;
	}
	else if(t == "del") {
		t.cmdval = Commands::del;
	}
	else if(t == "wait") {
		t.cmdval = Commands::wait;
	}
	else if(t == "call") {
		t.cmdval = Commands::call;
	}
	else if(t == "ret") {
		t.cmdval = Commands::ret;
	}
	else if(t == "time") {
		t.cmdval = Commands::dtime;
	}
	else if(t == "col") {
		t.cmdval = Commands::col;
	}
	else if(t == "pow") {
		t.cmdval = Commands::dpow;
	}
	else if(t == "rand") {
		t.cmdval = Commands::drand;
	}
	else if(t == "srand") {
		t.cmdval = Commands::dsrand;
	}
	else if(t == "int") {
		t.cmdval = Commands::intr;
	}
	else if(t == "kill") {
		t.cmdval = Commands::kill;
	}
	else if(t == "ldimg") {
		t.cmdval = Commands::ldimg;
	}
	else if(t == "img") {
		t.cmdval = Commands::img;
	}
	else if(t == "frimg") {
		t.cmdval = Commands::frimg;
	}
	else if(t == "alloc") {
		t.cmdval = Commands::aalloc;
	}
	else if(t == "free") {
		t.cmdval = Commands::afree;
	}
	else if(t == "get") {
		t.cmdval = Commands::aget;
	}
	else if(t == "set") {
		t.cmdval = Commands::aset;
	}
	else if(t == "len") {
		t.cmdval = Commands::alen;
	}
	else {
		t.cmdval = Commands::unknown;
	}
}