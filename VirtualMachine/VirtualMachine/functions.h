#pragma once

#include <string>
#include <vector>
#include <map>
#include <iostream>

using namespace std;

#define XERROR(m) cout << m << endl; exec = false; run_loop = false;
#define SERROR(c, g) cout << "Expected ';' after " << c << ", got " << g.tok_value << endl; exec = false; run_loop = false;
#define T_EQ(t, s) (t.tok_value == s)

#define variable map<string, double>::iterator
#define constant map<string, token>::iterator
#define label map<string, int>::iterator

void fdef();
void finc();
void fdec();
void fconst();
void fld();
void fadd();
void fsub();
void feq();
void fneq();
void fprint();
void fputc();
void fflush();
void flth();
void fgth();
void fmul();
void fdiv();
void fmod();
void flshift();
void frshift();
void flor();
void fland();
void flxor();
void fband();
void fbor();
void fbxor();
void flnot();
void fbnot();
void fsqrt();
void fsin();
void fcos();
void ftan();
void fasin();
void facos();
void fatan();
void fatan2();
void fpolk();
void fjmp();
void fif();
void fend();
void fdel();
void fwait();
void fcall();
void fret();
void ftime();
void fcol();
void fpow();