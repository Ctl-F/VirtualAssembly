#pragma once

#include <string>
#include <vector>
#include <cstdlib>
#include <iostream>

using std::vector;
using std::string;
using std::atof;
using std::cout;
using std::endl;

#define TYPE_STRING 0
#define TYPE_NUMBER 1
#define TYPE_DIRECTIVE 2
#define TYPE_NONE 3

#define IS_NUMERIC(a) (48 <= a && a < 58)

struct token{
	unsigned char type;
	bool is_array = false;
	string tok_value;
	double dvalue;
	
	string array_index;
	unsigned char array_index_type;
};

vector<token> parse_asm(string feed);
token parse_directive(string &feed);
token parse_identifier(string &feed);
token parse_number(string &feed);
bool parse_whitespace(string &feed);
bool parse_comment(string &feed);