#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <stack>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <cmath>
#include <time.h>

using namespace std;
using namespace std::chrono;

// TODO: (maybe?) Scope Based Functions

#define ARGUMENT_NUMBER 0
#define ARGUMENT_VARIABLE 1
#define ARGUMENT_DIRECTIVE 2
#define ARGUMENT_VARIABLENUMBER 3
#define ARGUMENT_ANY 4

#define ERROR_SEMICOLON 0
#define ERROR_INVALID_ARGUMENT 1
#define ERROR_INVALID_COMMAND 2


#include <allegro5\allegro.h>
#include <allegro5\allegro_primitives.h>

#include "parsing.h"

#define STD_IMPORT_PATH ".\\import\\"

#define INDEX(a, b, w) (b*w+a)
#define AT(a, b) INDEX(a, b, width)

#define FPS 120.

ALLEGRO_DISPLAY *main_window;
ALLEGRO_EVENT_QUEUE *main_eq;
ALLEGRO_TIMER *timer;

string local_import_path;

string usercode, outputbuff;
token *tokens;
unsigned int token_size;
int tindex, width, height, scale;
bool suspended = false, run_loop = true, exec = true;
int interupt_count = 0;
long delay = 0;
unsigned int *pixels;
unordered_map<string, double> variables;
unordered_map<string, token> constants;
unordered_map<string, int> label_op;
stack<int> call_stack;

bool key_states[ALLEGRO_KEY_MAX];

void prepopulate_jump_locations();
void handle_imports(string &feed);
bool validate_tokens();
void render();

#define XERROR(m) cout << m << endl; exec = false; run_loop = false;
#define SERROR(c, g) cout << "Expected ';' after " << c << ", got " << g.tok_value << endl; exec = false; run_loop = false;
#define T_EQ(t, s) (t.tok_value == s)

#define variable unordered_map<string, double>::iterator
#define constant unordered_map<string, token>::iterator
#define label unordered_map<string, int>::iterator

long current_time() {
	return duration_cast<nanoseconds> (
		system_clock::now().time_since_epoch()
		).count();
}

string get_path(string fname);

void fdef();
void finc();
void fdec();
void fconst();
void fld();
void fadd();
void fsub();
void feq();
void fneq();
void fprint();
void fputc();
void fflush();
void flth();
void fgth();
void fmul();
void fdiv();
void fmod();
void flshift();
void frshift();
void flor();
void fland();
void flxor();
void fband();
void fbor();
void fbxor();
void flnot();
void fbnot();
void fsqrt();
void fsin();
void fcos();
void ftan();
void fasin();
void facos();
void fatan();
void fatan2();
void fpolk();
void fjmp();
void fif();
void fend();
void fdel();
void fwait();
void fcall();
void fret();
void ftime();
void fcol();
void fpow();
void frand();
void fsrand();

int main(int argc, char **argv) {
	--argc; ++argv;
	usercode = "";
	outputbuff = "";

	for(int i = 0; i < ALLEGRO_KEY_MAX; i++) {
		key_states[i] = false;
	}

	if(argc > 0) {
		fstream ifile(argv[0]);
		if(ifile.is_open()) {
			string line;
			while(getline(ifile, line)) {
				usercode += line + "\n";
			}
			ifile.close();
		}
		else {
			printf("Could not open file %s\n", argv[0]);
			return 1;
		}
	}
	//cout << "User Code:\n" << usercode << endl;

	local_import_path = get_path(argv[0]);
	cout << "Local Path: " << local_import_path << endl;

	if(!al_init()) {
		printf("Could not initialize display\n");
		return 1;
	}
	
	handle_imports(usercode);

	vector<token> _tokens = parse_asm(usercode);
	token_size = _tokens.size();
	tokens = new token[token_size];
	for(int i = 0; i < token_size; i++) {
		tokens[i] = _tokens[i];
	}

	if(token_size == 0) {
		cout << "Null Input Error\n";
		return 1;
	}

	for(int i = 0; i < token_size; i++) {
		if(tokens[i].tok_value == "_TARGET_GML") {
			tokens[i].tok_value = "0";
			tokens[i].dvalue = 0.0;
			tokens[i].type = TYPE_NUMBER;
		}
		else if(tokens[i].tok_value == "_TARGET_X86" || tokens[i].tok_value == "_TARGET_x86") {
			tokens[i].tok_value = "1";
			tokens[i].dvalue = 1.0;
			tokens[i].type = TYPE_NUMBER;
		}
	}

	width = (int) tokens[0].dvalue;
	height = (int) tokens[1].dvalue;
	scale = (int) tokens[2].dvalue;

	pixels = new unsigned int[width*height];

	prepopulate_jump_locations();
	cout << token_size << " tokens loaded\n";
	tindex = 3;

	main_window = al_create_display(width*scale, height*scale);
	main_eq = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS);

	al_init_primitives_addon();

	al_install_keyboard();
	//al_install_mouse();

	al_register_event_source(main_eq, al_get_display_event_source(main_window));
	al_register_event_source(main_eq, al_get_timer_event_source(timer));
	al_register_event_source(main_eq, al_get_keyboard_event_source());
	//al_register_event_source(main_eq, al_get_mouse_event_source());

	al_start_timer(timer);
	while(run_loop) {
		ALLEGRO_EVENT ev;
		al_wait_for_event(main_eq, &ev);

		switch(ev.type) {
		case ALLEGRO_EVENT_TIMER: {
			render();
			if(suspended) break;

			exec = true;
			long _time = current_time();
			while(exec) {
				if(tindex >= token_size) {
					XERROR("Unexpected end of token feed");
					break;
				}

				token tok = tokens[tindex++];

				if(T_EQ(tok, "def")) {
					fdef();
				}
				else if(T_EQ(tok, "inc")) {
					finc();
				}
				else if(T_EQ(tok, "dec")) {
					fdec();
				}
				else if(T_EQ(tok, "const")) {
					fconst();
				}
				else if(T_EQ(tok, "int")) {
					if(T_EQ(tokens[tindex], ";")) {
						exec = false;
						interupt_count++;
						tindex++;
					}
					else {
						SERROR("int", tokens[tindex]);
					}
				}
				else if(T_EQ(tok, "kill")) {
					if(T_EQ(tokens[tindex], ";")) {
						exec = false;
						run_loop = false;
					}
					else {
						cout << "Expected ';' after kill, got " << tokens[tindex].tok_value;
						XERROR("");
					}
				}
				else if(T_EQ(tok, "ld")) {
					fld();
				}
				else if(T_EQ(tok, "add")) {
					fadd();
				}
				else if(T_EQ(tok, "sub")) {
					fsub();
				}
				else if(T_EQ(tok, "eq")) {
					feq();
				}
				else if(T_EQ(tok, "neq")) {
					fneq();
				}
				else if(T_EQ(tok, "print")) {
					fprint();
				}
				else if(T_EQ(tok, "putc")) {
					fputc();
				}
				else if(T_EQ(tok, "flush")) {
					//fflush();
					tindex++;
				}
				else if(T_EQ(tok, "lth")) {
					flth();
				}
				else if(T_EQ(tok, "gth")) {
					fgth();
				}
				else if(T_EQ(tok, "mul")) {
					fmul();
				}
				else if(T_EQ(tok, "div")) {
					fdiv();
				}
				else if(T_EQ(tok, "mod")) {
					fmod();
				}
				else if(T_EQ(tok, "lshift")) {
					flshift();
				}
				else if(T_EQ(tok, "rshift")) {
					frshift();
				}
				else if(T_EQ(tok, "or")) {
					flor();
				}
				else if(T_EQ(tok, "and")) {
					fland();
				}
				else if(T_EQ(tok, "xor")) {
					flxor();
				}
				else if(T_EQ(tok, "band")) {
					fband();
				}
				else if(T_EQ(tok, "bor")) {
					fbor();
				}
				else if(T_EQ(tok, "bxor")) {
					fbxor();
				}
				else if(T_EQ(tok, "not")) {
					flnot();
				}
				else if(T_EQ(tok, "bnot")) {
					fbnot();
				}
				else if(T_EQ(tok, "sqrt")) {
					fsqrt();
				}
				else if(T_EQ(tok, "sin")) {
					fsin();
				}
				else if(T_EQ(tok, "cos")) {
					fcos();
				}
				else if(T_EQ(tok, "tan")) {
					ftan();
				}
				else if(T_EQ(tok, "asin")) {
					fasin();
				}
				else if(T_EQ(tok, "acos")) {
					facos();
				}
				else if(T_EQ(tok, "atan")) {
					fatan();
				}
				else if(T_EQ(tok, "atan2")) {
					fatan2();
				}
				else if(T_EQ(tok, "polk")) {
					fpolk();
				}
				else if(T_EQ(tok, "jmp")) {
					fjmp();
				}
				else if(T_EQ(tok, "if")) {
					fif();
				}
				else if(T_EQ(tok, "end")) {
					fend();
				}
				else if(T_EQ(tok, "del")) {
					fdel();
				}
				else if(T_EQ(tok, "wait")) {
					fwait();
				}
				else if(T_EQ(tok, "call")) {
					fcall();
				}
				else if(T_EQ(tok, "ret")) {
					fret();
				}
				else if(T_EQ(tok, "time")) {
					ftime();
				}
				else if(T_EQ(tok, "col")) {
					fcol();
				}
				else if(T_EQ(tok, "pow")) {
					fpow();
				}
				else if(T_EQ(tok, "rand")) {
					frand();
				}
				else if(T_EQ(tok, "srand")) {
					fsrand();
				}
				else {

				}

				long _t = current_time();
				while(current_time() - _t < delay) {
				}
			}
			break;
		}
		case ALLEGRO_EVENT_KEY_DOWN: {
			key_states[ev.keyboard.keycode] = true;
			if(ev.keyboard.keycode == ALLEGRO_KEY_F3) {
				suspended = !suspended;
			}
			else if(ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
				run_loop = false;
			}
			break;
		}
		case ALLEGRO_EVENT_KEY_UP: {
			key_states[ev.keyboard.keycode] = false;
			break;
		}
		case ALLEGRO_EVENT_DISPLAY_CLOSE: {
			run_loop = false;
			break;
		}
		}
	}
	al_stop_timer(timer);

	//unordered_map<string, double>::iterator i = variables.begin();
	//for(; i != variables.end(); i++) {
	//	cout << i->first << ": " << i->second << endl;
	//}

	al_destroy_event_queue(main_eq);
	al_destroy_timer(timer);
	al_destroy_display(main_window);

	delete[] tokens;

	return 0;
}

string get_path(string fname) {
	int lpos = fname.find_last_of('\\');
	if(lpos == string::npos) {
		lpos = fname.find_last_of("/");
		if(lpos == string::npos) {
			cout << "Error retrieveing path of " << fname << endl;
		}
	}
	return fname.substr(0, lpos + (lpos!=string::npos?1:0));
}

void render(){
	for(int i = 0; i < width; i++) {
		for(int j = 0; j < height; j++) {
			int r, g, b;
			r = ((pixels[AT(i, j)]) >> 16) & 0b11111111;
			g = ((pixels[AT(i, j)]) >> 8) & 0b11111111;
			b = pixels[AT(i, j)] & 0b11111111;
			al_draw_filled_rectangle(i*scale, j*scale, (i + 1)*scale, (j + 1)*scale, al_map_rgb(r, g, b));
		}
	}
	al_flip_display();
}

void handle_imports(string &feed) {
	int pos;
	while((pos = feed.find(".import")) != string::npos) {
		string fname = "";
		for(int i = pos + 8; i < feed.length(); i++) {
			char c = feed[i];
			if(c == ';') {
				break;
			}
			fname += c;
		}

		int s = 0; // normal

		if(fname.substr(0, min(fname.length(), 4)) == "std:") {
			fname.replace(0, 4, STD_IMPORT_PATH);
			fname += ".a";
			s = 1; // std;
		}
		else if(fname.substr(0, min(fname.length(), 2)) == ".\\" || fname.substr(0, min(fname.length(), 2)) == "./") {
			fname.replace(0, 2, local_import_path);
			s = 2; // local;
		}

		cout << "fetching contents of " << fname << endl;

		string content = "";
		fstream ifile(fname);
		if(ifile.is_open()) {
			string line;
			while(getline(ifile, line)) {
				content += line + "\n";
			}
			ifile.close();
		}
		else {
			cout << "Could not import " << fname << "\n";
		}
		feed = feed.replace(pos, 8 + (s==0?fname.length():(s==1?4:2)), content);
	}
}
void prepopulate_jump_locations() {
	for(int i = 0; i < token_size; i++) {
		if(tokens[i].type == TYPE_DIRECTIVE) {
			//tokens[i].tok_value == dir.tok_value && (i - 1 < 3 || T_EQ(tokens[i - 1], ";"))
			if(i - 1 < 3 || T_EQ(tokens[i - 1], ";")) {
				for(int j = i; j < token_size; j++) {
					if(T_EQ(tokens[j], ";")) {
						label_op[tokens[i].tok_value] = j + 1;
						break;
					}
				}
			}
		}
	}
}

// NOTE: start_index is the index of the command plus 1
bool validate_command_noarg(int start_index) {
	if(T_EQ(tokens[start_index], ";")) {
		return true;
	}
	return false;
}

bool validate_command_1arg(int start_index, byte argument_type) {
	return true;
}

bool validate_tokens() {
	return true;
}

void fdef() {
	token name = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variables[name.tok_value] = 0.0;
			tindex++;
		}
		else {
			XERROR("Expected identifier after def");
			return;
		}
	}
	else {
		cout << "Expected ';' after def, got " << tokens[tindex].tok_value;
		XERROR("");
		return;
	}
}
void finc() {
	token name = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable entry = variables.find(name.tok_value);
			if(entry != variables.end()) {
				entry->second += 1.0;
				tindex++;
			}
			else {
				cout << "Unknown Variable " << name.tok_value;
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected identifier after inc");
			return;
		} 
	}
	else {
		cout << "Expected ';' after inc, got " << tokens[tindex].tok_value;
		XERROR("");
		return;
	}
}
void fdec() {
	token name = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable entry = variables.find(name.tok_value);
			if(entry != variables.end()) {
				entry->second -= 1.0;
				tindex++;
			}
			else {
				cout << "Unknown Variable " << name.tok_value;
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected identifier after dec");
			return;
		}
	}
	else {
		cout << "Expected ';' after dec, got " << tokens[tindex].tok_value;
		XERROR("");
		return;
	}
}
void fconst() {
	int cmd_index = tindex - 1;
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			constants[name.tok_value] = value;
			//tokens.erase(tokens.begin()+cmd_index, tokens.begin()+cmd_index+3); Heavy Operation
			tokens[cmd_index].tok_value = ";";
			tokens[cmd_index + 1].tok_value = ";";
			tokens[cmd_index + 2].tok_value = ";";
			//tindex -= 3;
			for(int i = 3; i < token_size; i++) {
				if(tokens[i].tok_value == name.tok_value) {
					tokens[i] = value;
				}
			}
			tindex++;
		}
		else {
			XERROR("Expected string after const");
		}
	}
	else {
		SERROR("const", tokens[tindex]);
	}
}
void fld() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable n = variables.find(name.tok_value);
			if(n != variables.end()) {
				if(value.type == TYPE_STRING) {
					variable v = variables.find(value.tok_value);
					if(v != variables.end()) {
						n->second = v->second;
						tindex++;
					}
					else {
						cout << value.tok_value << "is undefined";
						XERROR("");
					}
				}
				else {
					n->second = value.dvalue;
				}
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected identifier after ld");
		}
	}
	else {
		SERROR("ld", tokens[tindex]);
		return;
	}
}
void fadd() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER){
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to add");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to add");
					return;
				}

				v->second = lval + rval;
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after add");
		}
	}
	else {
		SERROR("add", tokens[tindex]);
	}
}
void fsub() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(v != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to sub");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(v != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to sub");
					return;
				}

				v->second = lval - rval;
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after sub");
		}
	}
	else {
		SERROR("sub", tokens[tindex]);
	}
}
void feq() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to eq");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(v != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to eq");
					return;
				}

				v->second = (double)(lval == rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after eq");
		}
	}
	else {
		SERROR("eq", tokens[tindex]);
	}
}
void fneq() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to neq");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to neq");
					return;
				}

				v->second = (double) (lval != rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after neq");
		}
	}
	else {
		SERROR("neq", tokens[tindex]);
	}
}
void fprint() {
	token val = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(val.type == TYPE_STRING) {
			variable v = variables.find(val.tok_value);
			if(v != variables.end()) {
				cout << v->second << endl;
				tindex++;
			}
			else {
				cout << val.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			cout << val.tok_value << endl;
			tindex++;
		}
	}
	else {
		SERROR("print", tokens[tindex]);
	}
}
void fputc() {
	token val = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(val.type == TYPE_STRING) {
			variable v = variables.find(val.tok_value);
			if(v != variables.end()) {
				putchar((char) v->second);
				tindex++;
			}
			else {
				cout << val.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else if(val.type == TYPE_NUMBER) {
			putchar((char) val.dvalue);
			tindex++;
		}
		else {
			XERROR("Invalid character input for putc");
		}
	}
	else {
		SERROR("putc", tokens[tindex]);
	}
}
void fflush() {
	if(T_EQ(tokens[tindex], ";")) {
		printf("%s -> %i", outputbuff.c_str(), outputbuff.length());
		outputbuff = "";
		tindex++;
	}
	else {
		SERROR("flush", tokens[tindex]);
	}
}
void flth() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to lth");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to lth");
					return;
				}

				v->second = (double)(lval < rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after lth");
		}
	}
	else {
		SERROR("lth", tokens[tindex]);
	}
}
void fgth() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to gth");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to gth");
					return;
				}

				v->second = (double) (lval > rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after gth");
		}
	}
	else {
		SERROR("gth", tokens[tindex]);
	}
}
void fmul() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to mul");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to mul");
					return;
				}

				v->second = lval * rval;
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after mul");
		}
	}
	else {
		SERROR("mul", tokens[tindex]);
	}
}
void fdiv() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to div");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to div");
					return;
				}

				v->second = lval / rval;
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after div");
		}
	}
	else {
		SERROR("div", tokens[tindex]);
	}
}
void fmod() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to mod");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to mod");
					return;
				}

				v->second = (double)((long)lval % (long)rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after mod");
		}
	}
	else {
		SERROR("mod", tokens[tindex]);
	}
}
void flshift() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to lshift");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to lshift");
					return;
				}

				v->second = (double)((long)lval << (long)rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after lshift");
		}
	}
	else {
		SERROR("lshift", tokens[tindex]);
	}
}
void frshift() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to rshift");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to rshift");
					return;
				}

				v->second = (double) ((long) lval >> (long) rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after rshift");
		}
	}
	else {
		SERROR("rshift", tokens[tindex]);
	}
}
void flor() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to or");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to or");
					return;
				}

				v->second = (lval || rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after or");
		}
	}
	else {
		SERROR("or", tokens[tindex]);
	}
}
void fland() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					variable s = variables.find(lh.tok_value);
					if(s != variables.end()) {
						lval = s->second;
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to and");
					return;
				}

				if(rh.type == TYPE_STRING) {
					variable s = variables.find(rh.tok_value);
					if(s != variables.end()) {
						rval = s->second;
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to and");
					return;
				}

				v->second = (lval && rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after and");
		}
	}
	else {
		SERROR("and", tokens[tindex]);
	}
}
void flxor() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					v = variables.find(lh.tok_value);
					if(v != variables.end()) {
						lval = variables[lh.tok_value];
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to xor");
					return;
				}

				if(rh.type == TYPE_STRING) {
					v = variables.find(rh.tok_value);
					if(v != variables.end()) {
						rval = variables[rh.tok_value];
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to xor");
					return;
				}

				variables[var.tok_value] = !((lval && rval) || (!lval && !rval));
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after xor");
		}
	}
	else {
		SERROR("xor", tokens[tindex]);
	}
}
void fband() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					v = variables.find(lh.tok_value);
					if(v != variables.end()) {
						lval = variables[lh.tok_value];
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to band");
					return;
				}

				if(rh.type == TYPE_STRING) {
					v = variables.find(rh.tok_value);
					if(v != variables.end()) {
						rval = variables[rh.tok_value];
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to band");
					return;
				}

				variables[var.tok_value] = (double) ((long) lval & (long) rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after band");
		}
	}
	else {
		SERROR("band", tokens[tindex]);
	}
}
void fbor() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					v = variables.find(lh.tok_value);
					if(v != variables.end()) {
						lval = variables[lh.tok_value];
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to bor");
					return;
				}

				if(rh.type == TYPE_STRING) {
					v = variables.find(rh.tok_value);
					if(v != variables.end()) {
						rval = variables[rh.tok_value];
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to bor");
					return;
				}

				variables[var.tok_value] = (double) ((long) lval | (long) rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after bor");
		}
	}
	else {
		SERROR("bor", tokens[tindex]);
	}
}
void fbxor() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					v = variables.find(lh.tok_value);
					if(v != variables.end()) {
						lval = variables[lh.tok_value];
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to bxor");
					return;
				}

				if(rh.type == TYPE_STRING) {
					v = variables.find(rh.tok_value);
					if(v != variables.end()) {
						rval = variables[rh.tok_value];
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to bxor");
					return;
				}

				variables[var.tok_value] = (double) ((long) lval ^ (long) rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after bxor");
		}
	}
	else {
		SERROR("bxor", tokens[tindex]);
	}
}
void flnot() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for not");
					return;
				}

				variables[name.tok_value] = (double) !val;
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after not");
		}
	}
	else {
		SERROR("not", tokens[tindex]);
	}
}
void fbnot() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for bnot");
					return;
				}

				variables[name.tok_value] = (double) ~(long)val;
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after bnot");
		}
	}
	else {
		SERROR("bnot", tokens[tindex]);
	}
}
void fsqrt() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for sqrt");
					return;
				}

				variables[name.tok_value] = sqrt(val);
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after sqrt");
		}
	}
	else {
		SERROR("sqrt", tokens[tindex]);
	}
}
void fsin() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for sin");
					return;
				}

				variables[name.tok_value] = sin(val);
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after sin");
		}
	}
	else {
		SERROR("sin", tokens[tindex]);
	}
}
void fcos() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for cos");
					return;
				}

				variables[name.tok_value] = cos(val);
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after cos");
		}
	}
	else {
		SERROR("cos", tokens[tindex]);
	}
}
void ftan() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for tan");
					return;
				}

				variables[name.tok_value] = tan(val);
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after tan");
		}
	}
	else {
		SERROR("tan", tokens[tindex]);
	}
}
void fasin() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for asin");
					return;
				}

				variables[name.tok_value] = asin(val);
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after asin");
		}
	}
	else {
		SERROR("asin", tokens[tindex]);
	}
}
void facos() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for acos");
					return;
				}

				variables[name.tok_value] = acos(val);
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after acos");
		}
	}
	else {
		SERROR("acos", tokens[tindex]);
	}
}
void fatan() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for atan");
					return;
				}

				variables[name.tok_value] = atan(val);
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after atan");
		}
	}
	else {
		SERROR("atan", tokens[tindex]);
	}
}
void fatan2() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					v = variables.find(lh.tok_value);
					if(v != variables.end()) {
						lval = variables[lh.tok_value];
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to atan2");
					return;
				}

				if(rh.type == TYPE_STRING) {
					v = variables.find(rh.tok_value);
					if(v != variables.end()) {
						rval = variables[rh.tok_value];
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to atan2");
					return;
				}

				variables[var.tok_value] = atan2(rval, lval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after atan2");
		}
	}
	else {
		SERROR("atan2", tokens[tindex]);
	}
}
void fpolk() {
	token name = tokens[tindex++], value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				double val;
				if(value.type == TYPE_STRING) {
					v = variables.find(value.tok_value);
					if(v != variables.end()) {
						val = variables[value.tok_value];
					}
					else {
						cout << value.tok_value << " is undefined";
						XERROR("");
					}
				}
				else if(value.type == TYPE_NUMBER) {
					val = value.dvalue;
				}
				else {
					XERROR("Invalid type for polk");
					return;
				}

				variables[name.tok_value] = (double) key_states[(long) val];
			}
			else {
				cout << name.tok_value << " is undefined";
				XERROR("");
			}
		}
		else {
			XERROR("Expected string after polk");
		}
	}
	else {
		SERROR("polk", tokens[tindex]);
	}
}
void fjmp() {
	token dir = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(dir.type == TYPE_DIRECTIVE) {
			label l = label_op.find(dir.tok_value);
			if(l != label_op.end()) {
				// found place to jump already
				tindex = l->second;
				return;
			}
			else {
				cout << "slow route\n";
				int bind = -1;
				for(int i = tindex - 2; i > 2; i--) {
					if(tokens[i].tok_value == dir.tok_value && (i - 1 < 3 || T_EQ(tokens[i - 1], ";"))) {
						bind = i;
						break;
					}
				}

				if(bind == -1) {
					for(int i = tindex + 1; i < token_size; i++) {
						if(tokens[i].tok_value == dir.tok_value && (i - 1 < 3 || T_EQ(tokens[i - 1], ";"))) {
							bind = i;
							break;
						}
					}
					if(bind == -1) {
						cout << "Directive " << dir.tok_value << " does not exist";
						XERROR("");
						return;
					}
					else {
						for(int i = bind; i < token_size; i++) {
							if(T_EQ(tokens[i], ";")) {
								tindex = i + 1;
								label_op[dir.tok_value] = i + 1;
								return;
							}
						}
						cout << "Expected ';' after directive(" << dir.tok_value << ")";
						XERROR(0);
					}
				}
				else {
					for(int i = bind; i < token_size; i++) {
						if(T_EQ(tokens[i], ";")) {
							tindex = i + 1;
							label_op[dir.tok_value] = i + 1;
							return;
						}
					}
					cout << "Expected ';' after directive(" << dir.tok_value << ")";
					XERROR(0);
				}
			}
		}
		else {
			XERROR("Expected directive after jmp");
		}
	}
	else {
		SERROR("jmp", tokens[tindex]);
	}
}
void fif() {
	token val = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		double condition;
		if(val.type == TYPE_STRING) {
			variable v = variables.find(val.tok_value);
			if(v != variables.end()) {
				condition = variables[val.tok_value];
			}
			else {
				cout << val.tok_value << " is undefined";
				XERROR("");
			}
		}
		else if(val.type == TYPE_NUMBER) {
			condition = val.dvalue;
		}
		else {
			XERROR("Invalid type for if");
			return;
		}

		if(!condition){
			int icount = 1;
			for(int i = tindex + 1; i < token_size; i++) {
				if(T_EQ(tokens[i], "if")) {
					icount++;
				}
				else if(T_EQ(tokens[i], "end")) {
					icount--;
				}
				if(T_EQ(tokens[i], ";") && !icount) {
					tindex = i + 1;
					break;
				}
			}
			if(icount > 0) {
				XERROR("Could not find paired 'end' command for 'if'");
			}
		}
		else {
			tindex++;
		}
	}
	else {
		SERROR("if", tokens[tindex]);
	}
}
void fend() {
	if(T_EQ(tokens[tindex], ";")) {
		tindex++;
	}
	else {
		SERROR("end", tokens[tindex]);
	}
}
void fdel() {
	token name = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable v = variables.find(name.tok_value);
			if(v != variables.end()) {
				variables.erase(name.tok_value);
				tindex++;
			}
			else {
				cout << "Trying to delete non-existant variable " << name.tok_value;
				XERROR("");
			}
		}
		else {
			XERROR("Expected variable name after del");
		}
	}
	else {
		SERROR("del", tokens[tindex]);
	}
}
void fwait() {
	token name = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable entry = variables.find(name.tok_value);
			if(entry != variables.end()) {
				long span = (long) variables[name.tok_value];
				long time = current_time();
				while(current_time() - time < span) { }
				tindex++;
				return;
			}
			else {
				cout << "Unknown Variable " << name.tok_value;
				XERROR("");
				return;
			}
		}
		else if(name.type == TYPE_NUMBER) {
			long span = (long) name.dvalue;
			long time = current_time();
			while(current_time() - time < span) { }
			tindex++;
			return;
		}
		else {
			XERROR("Expected variable or number after wait");
			return;
		}
	}
	else {
		cout << "Expected ';' after wait, got " << tokens[tindex].tok_value;
		XERROR("");
		return;
	}
}
void fcall() {
	token dir = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(dir.type == TYPE_DIRECTIVE) {
			label l = label_op.find(dir.tok_value);
			if(l != label_op.end()) {
				// found place to jump already
				call_stack.push(tindex + 1);
				tindex = label_op[dir.tok_value];
				return;
			}
			else {
				cout << "slow route\n";
				int bind = -1;
				for(int i = tindex - 2; i > 2; i--) {
					if(tokens[i].tok_value == dir.tok_value && (i - 1 < 3 || T_EQ(tokens[i - 1], ";"))) {
						bind = i;
						break;
					}
				}

				if(bind == -1) {
					for(int i = tindex + 1; i < token_size; i++) {
						if(tokens[i].tok_value == dir.tok_value && (i - 1 < 3 || T_EQ(tokens[i - 1], ";"))) {
							bind = i;
							break;
						}
					}
					if(bind == -1) {
						cout << "Directive " << dir.tok_value << " does not exist";
						XERROR("");
						return;
					}
					else {
						for(int i = bind; i < token_size; i++) {
							if(T_EQ(tokens[i], ";")) {
								call_stack.push(tindex + 1);
								tindex = i + 1;
								label_op[dir.tok_value] = i + 1;
								return;
							}
						}
						cout << "Expected ';' after directive(" << dir.tok_value << ")";
						XERROR(0);
					}
				}
				else {
					for(int i = bind; i < token_size; i++) {
						if(T_EQ(tokens[i], ";")) {
							call_stack.push(tindex + 1);
							tindex = i + 1;
							label_op[dir.tok_value] = i + 1;
							return;
						}
					}
					cout << "Expected ';' after directive(" << dir.tok_value << ")";
					XERROR(0);
				}
			}
		}
		else {
			XERROR("Expected directive after call");
		}
	}
	else {
		SERROR("call", tokens[tindex]);
	}
}
void fret() {
	if(T_EQ(tokens[tindex], ";")) {
		if(call_stack.size() > 0) {
			tindex = call_stack.top();
			call_stack.pop();
		}
		else {
			XERROR("Unexpected 'ret' command");
		}
	}
	else {
		SERROR("ret", tokens[tindex]);
	}
}
void ftime() {
	token name = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable entry = variables.find(name.tok_value);
			if(entry != variables.end()) {
				variables[name.tok_value] = (double) current_time();
				tindex++;
			}
			else {
				cout << "Unknown Variable " << name.tok_value;
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected identifier after time");
			return;
		}
	}
	else {
		cout << "Expected ';' after time, got " << tokens[tindex].tok_value;
		XERROR("");
		return;
	}
}
void fcol() {
	token xv = tokens[tindex++], yv = tokens[tindex++], colr = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		double x, y, color;
		if(xv.type == TYPE_STRING) {
			variable v = variables.find(xv.tok_value);
			if(v != variables.end()) {
				x = v->second;
			}
			else {
				cout << xv.dvalue << " is undefined";
				XERROR("");
				return;
			}
		}
		else if(xv.type == TYPE_NUMBER) {
			x = xv.dvalue;
		}
		else {
			XERROR("Expected variable or number after 'col'");
			return;
		}

		if(yv.type == TYPE_STRING) {
			variable v = variables.find(yv.tok_value);
			if(v != variables.end()) {
				y = v->second;
			}
			else {
				cout << yv.dvalue << " is undefined";
				XERROR("");
				return;
			}
		}
		else if(yv.type == TYPE_NUMBER) {
			y = yv.dvalue;
		}
		else {
			XERROR("Expected variable or number after 'col'");
			return;
		}

		if(colr.type == TYPE_STRING) {
			variable v = variables.find(colr.tok_value);
			if(v != variables.end()) {
				color = v->second;
			}
			else {
				cout << colr.dvalue << " is undefined";
				XERROR("");
				return;
			}
		}
		else if(colr.type == TYPE_NUMBER) {
			color = colr.dvalue;
		}
		else {
			XERROR("Expected variable or number after 'col'");
			return;
		}

		if(0 <= AT(x, y) && AT(x, y) < (width*height)) {
			pixels[(long) AT(x, y)] = (unsigned int) color;
		}
		tindex++;
	}
	else {
		SERROR("col", tokens[tindex]);
	}
}
void fpow() {
	token var = tokens[tindex++], lh = tokens[tindex++], rh = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(var.type == TYPE_STRING) {
			variable v = variables.find(var.tok_value);
			if(v != variables.end()) {
				double lval, rval;
				if(lh.type == TYPE_STRING) {
					v = variables.find(lh.tok_value);
					if(v != variables.end()) {
						lval = variables[lh.tok_value];
					}
					else {
						cout << lh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(lh.type == TYPE_NUMBER) {
					lval = lh.dvalue;
				}
				else {
					XERROR("Invalid left operand to pow");
					return;
				}

				if(rh.type == TYPE_STRING) {
					v = variables.find(rh.tok_value);
					if(v != variables.end()) {
						rval = variables[rh.tok_value];
					}
					else {
						cout << rh.tok_value << " is undefined";
						XERROR("");
						return;
					}
				}
				else if(rh.type == TYPE_NUMBER) {
					rval = rh.dvalue;
				}
				else {
					XERROR("Invalid right operand to pow");
					return;
				}

				variables[var.tok_value] = pow(lval, rval);
				tindex++;
			}
			else {
				cout << var.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected variable after pow");
		}
	}
	else {
		SERROR("pow", tokens[tindex]);
	}
}
void frand() {
	token name = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(name.type == TYPE_STRING) {
			variable entry = variables.find(name.tok_value);
			if(entry != variables.end()) {
				entry->second = (double) rand();
				tindex++;
			}
			else {
				cout << "Unknown Variable " << name.tok_value;
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Expected identifier after rand");
			return;
		}
	}
	else {
		cout << "Expected ';' after rand, got " << tokens[tindex].tok_value;
		XERROR("");
		return;
	}
}
void fsrand() {
	token value = tokens[tindex++];
	if(T_EQ(tokens[tindex], ";")) {
		if(value.type == TYPE_NUMBER) {
			srand((unsigned int) value.dvalue);
			tindex++;
		}
		else if(value.type == TYPE_STRING) {
			variable v = variables.find(value.tok_value);
			if(v != variables.end()) {
				srand((unsigned int) v->second);
			}
			else {
				cout << value.tok_value << " is undefined";
				XERROR("");
				return;
			}
		}
		else {
			XERROR("Invalid type for srand");
			return;
		}
	}
	else {
		SERROR("srand", tokens[tindex]);
		return;
	}
}