import time;
'''
    def ty nm vl; ---------------- 3;
    asn nm vl; ------------------- 2;
    add v1 v2; ------------------- 2;
    sub v2 v2; ------------------- 2;
    mul v1 v2; ------------------- 2;
    div v1 v2; ------------------- 2;
    mod v1 v2; ------------------- 2;
    bit v2 v2 op; ---------------- 3;
    jmp ln; ---------------------- 1;
    imp lib; --------------------- 1;
    mdl lib; --------------------- 1;
    del vl; ---------------------- 1;
    pdf vl; ---------------------- 1;
    chk v1 v2 st l1 l2; ---------- 5;
    exe cod; --------------------- 1;

    parsing needed:
        1 arg
        2 arg
        3 arg
        5 arg
        getVal from @notation
'''
# the allowed commands
comands = {
    'def' : 0,
    'asn' : 1,
    'add' : 2,
    'sub' : 3,
    'mul' : 4,
    'div' : 5,
    'mod' : 6,
    'bit' : 7,
    'jmp' : 8,
    'imp' : 9,
    'mdl' : 10,
    'del' : 11,
    'pdf' : 12,
    'chk' : 13,
    'exe' : 14
    };
'''
	
	def BOO foo @;
'''
# The following functions are various scripts you can call from the exe function (basically, I can't think of a better way for file i/o and console i/o
def exeCout():
    # console out
    print(sysVars['_ARG0_']);

def exeGetLine():
    #console getline
    sysVars["_RETURN_"] = input(sysVars["_ARG0_"]);

def exeGetChar():
    #console getchar
    sysVars["_RETURN_"] = input(sysVars["_ARG0_"])[0];

def exeGetReal():
    #console getreal (ints or floats);
    inpt = input(sysVars["_ARG0_"]);
    restr = "0";
    isFloat = False;
    for c in inpt:
        if c in "-0.123456789":
            restr += c;
        if(c == "."):
            isFloat = True;
    val = 0;
    if(isFloat):
        val = float(restr);
    else:
        val = int(restr);
    sysVars["_RETURN_"] = val;

def getRandom():
    t = time.time();
    return t % 1;

def exeWriteFile():
    # fname = _ARG0_
    # data = _ARG1_
    fname = sysVars["_ARG0_"];
    data = sysVars["_ARG1_"];
    f = open(fname, "w");
    f.write(data);
    f.close();

# These functions are what is called after parsing the command
# they are the actual functionality behind the command

#define command
def fasmDef(ty, nm, vl):
    global realVars, stringVars, boolVars;
    if(nm in realVars or nm in stringVars or nm in boolVars or nm in sysVars):
        print("Variable: " + nm + " already exists!");
        return -1;
    else:
        if(ty == "REA"):
            realVars[nm] = vl;
            return 1;
        elif(ty == "STR"):
            stringVars[nm] = vl;
            return 1;
        elif(ty == "BOO"):
            boolVars[nm] = vl;
            return 1;
        else:
            print("Unknown data type: " + ty);
            return -1;

#assign command
def fasmAsn(var, vl):
    global realVars, stringVars, sysVars, boolVars;
    if(var in realVars):
        realVars[var] = vl;
        return 1;
    elif(var in stringVars):
        stringVars[var] = vl;
        return 1;
    elif(var in boolVars):
        boolVars[var] = vl;
        return 1;
    elif(var in sysVars):
        sysVars[var] = vl;
        return 1;
    else:
        print("Unknown variable: " + var);
        return -1;

#add command
def fasmAdd(v1, v2):
    global sysVars;
    if(type(v1) == type("str") or type(v2) == type("str")):
        v1 = str(v1);
        v2 = str(v2);
    sysVars["_ADD_"] = v1+v2;
    return 1;

#subtract command
def fasmSub(v1, v2):
    global sysVars;
    sysVars["_SUB_"] = v1-v2;
    return 1;

#multiply command
def fasmMul(v1, v2):
    global sysVars;
    sysVars["_MUL_"] = v1*v2;
    return 1;

#divide command
def fasmDiv(v1, v2):
    global sysVars;
    sysVars["_DIV_"] = v1/v2;
    return 1;

#modulo command
def fasmMod(v1, v2):
    global sysVars;
    sysVars["_MOD_"] = v1%v2;
    return 1;

#bitwise command
def fasmBit(v1, v2, op):
    global sysVars;
    val = v1;
    if(op == "&"):
        val = v1 & v2;
    elif(op == "|"):
        val = v1 | v2;
    elif(op == "^"):
        val = v1 ^ v2;
    elif(op == ">>"):
        val = v1 >> v2;
    elif(op == "<<"):
        val = v1 << v2;
    elif(op == "!"):
        val = ~v1;
    else:
        print("Unknown operator: " + op);
        return -1;

    sysVars["_BIT_"] = val;
    return 1;

#jump command
def fasmJmp(line):
    global sysVars, currentLine;
    sysVars["_JPRV_"] = currentLine;
    currentLine = line-2;
    return 1;

#import command
def fasmImp(module):
    global modules;
    if not module in modules:
        modules[module] = [];
        f = open(".\\bin\\lib\\"+module, 'r');
        for line in f:
            modules[module].append(line);
        f.close();
    return 1;

#module command
def fasmMdl(module):
    global modules, currentLine;
    if(module in modules):
        previousLine = currentLine;
        currentLine = 0;
        lines = modules[module];
        debug = sysVars['_DEBUG_'];
        if(debug == 1):
            print("Entering module: " + module);
        while(True):
            if(not run(lines)):
                break;
        if(debug == 1):
            print("Returned");
        currentLine = previousLine;
        return 1;
    else:
        print("Unknown Module: " + module);
        return -1;
    
#delete command
def fasmDel(var):
    global realVars, stringVars, boolVars, modules;
    firstTry = 0;
    if(var in realVars):
        firstTry = realVars.pop(var, chr(0x10FFFF));
    elif var in stringVars:
        firstTry = stringVars.pop(var, chr(0x10FFFF));
    elif var in boolVars:
        firstTry = boolVars.pop(var, chr(0x10FFFF));
    elif var in modules:
        firstTry = modules.pop(var, chr(0x10FFFF));

    if(firstTry == chr(0x10FFFF)):
        print("Could not find the variable/module: " + var + " to delete.");
        return -1;
    return 1;

#'predefined' command
def fasmPdf(var):
    global realVars, stringVars, boolVars, sysVars, modules;
    t = False;
    if(var in realVars):
        t = True;
    elif(var in stringVars):
        t = True;
    elif(var in boolVars):
        t = True;
    elif(var in sysVars):
        t = True;
    elif(var in modules):
        t = True;
    sysVars["_VAREXISTS_"] = t;
    return 1;

#check command    
def fasmChk(v1, v2, op, lin1, lin2):
    if(op == "=="):
        if(v1 == v2):
            fasmJmp(lin1);
            return 1;
        fasmJmp(lin2);
        return 1;
    elif(op == ">="):
        if(v1 >= v2):
            fasmJmp(lin1);
            return 1;
        fasmJmp(lin2);
        return 1;
    elif(op == "<="):
        if(v1 <= v2):
            fasmJmp(lin1);
            return 1;
        fasmJmp(lin2);
        return 1;
    elif(op == ">"):
        if(v1 > v2):
            fasmJmp(lin1);
            return 1;
        fasmJmp(lin2);
        return 1;
    elif(op == "<"):
        if(v1 < v2):
            fasmJmp(lin1);
            return 1;
        fasmJmp(lin2);
        return 1;
    elif(op == "!="):
        if(v1 != v2):
            fasmJmp(lin1);
            return 1;
        fasmJmp(lin2);
        return 1;
    else:
        print("Unknown operator " + op);
        return -1;
    
#execute command (not implemented)
def fasmExe(cod):
    if(cod == "systemConsoleOutput"):
        exeCout();
        return 1;
    elif(cod == "systemConsoleGetLine"):
        exeGetLine();
        return 1;
    elif(cod == "systemConsoleGetChar"):
        exeGetChar();
        return 1;
    elif(cod == "systemConsoleGetReal"):
        exeGetReal();
        return 1;
    elif(cod == "systemRandom"):
        sysVars['_RAND_'] = getRandom();
        return 1;
    elif(cod == "systemWriteFile"):
        exeWriteFile();
        return 1;
    else:
        print("Unknown System Function: " + cod);
        return -1;

#FASM variables are just keys and the value of the variable is the value coresponding to the key
#in the dictionary.

#modules are stored as follows
#modules(dictionary)
#each module in the dictionary is the key, and the code is the value(a list);
#so
#modules[module] is the list([..., ..., ..., ...]) of commands
sysVars = {};
realVars = {};
stringVars = {};
boolVars = {};
modules = {};

#program lines is the list of the ./bin/main file (main program)
programLines = [];

#current line is the line that is being parsed that cycle
currentLine = 0;

#the init function defines all the default system values and loads the main program from ./bin/main
def init():
    global sysVars, programLines;
    
    sysVars['_ADD_'] = 0;
    sysVars['_SUB_'] = 0;
    sysVars['_MUL_'] = 0;
    sysVars['_DIV_'] = 0;
    sysVars['_BIT_'] = 0;
    sysVars['_RETURN_'] = 0;
    sysVars['_TEMP_'] = 0;
    sysVars['_ARG0_'] = 0;
    sysVars['_ARG1_'] = 0;
    sysVars['_ARG2_'] = 0;
    sysVars['_ARG3_'] = 0;
    sysVars['_ARG4_'] = 0;
    sysVars['_ARG5_'] = 0;
    sysVars['_JPRV_'] = 0;
    sysVars['_VAREXISTS_'] = 0;
    sysVars["_DEBUG_"] = 0;
    sysVars['true'] = 1;
    sysVars['false'] = 0;
    sysVars['_RAND_'] = getRandom();

    file = open(".\\bin\\main", 'r');
    for line in file:
        programLines.append(line);
    file.close();

parseArg0 = "";
parseArg1 = "";
parseArg2 = "";
parseArg3 = "";
parseArg4 = "";

# the next functions parse a line(string) for arguments from that string
# the string that gets passed INCLUDES THE COMMAND
# for it to parse correctly IT REQUIRES THE ENTIRE LINE
# "def REA foo 0;" -> will parse correctly REA(stored in parseArg0), foo(stored in parseArg1), and 0(stored in parseArg2);
# "REA foo 0;" -> WILL NOT PARSE CORRECTLY

def parseLineArg1(line):
    #parses arguments from a line assuming that it has one argument.
    #cmd X;
    global parseArg0;
    ps = 4;
    pe = 4;

    for c in line:
        if(c == ";"):
            parseArg0 = line[ps:(pe-ps)];
            break;
        else:
            pe += 1;

def parseLineArg2(line):
    #parses arguments from a line assuming that it has two arguments.
    #cmd x y;
    global parseArg0, parseArg1;
    ps = 4;
    pe = 4;
    charIndex = 0;
    inStr = False;
    openChar = " ";
    openIndex = len(line);
    for c in line:
        if(openChar == " " and (c == "'" or c == '"') and inStr == False):
            openChar = c;
            inStr = True;
            openIndex = charIndex
        if(c == openChar and (charIndex - openIndex > 0)):
            inStr = False;
        
        if(c == " " and (not inStr) and charIndex > 3):
            parseArg0 = line[ps:(pe-ps)];
            break;
        else:
            pe += 1;
            charIndex += 1;

    ps = pe-ps+1;
    pe = ps;
    
    for c in line:
        if(c == ";"):
            parseArg1 = line[ps:pe-ps];
            break;
        else:
            pe += 1;

def parseLineArg3(line):
    #parses arguments from a line assuming that it has two arguments.
    #cmd x y;
    global parseArg0, parseArg1, parseArg2;
    ps = 4;
    pe = 4;
    charIndex = 0;
    inStr = False;
    openChar = " ";
    openIndex = len(line);
    for c in line:
        if(openChar == " " and (c == "'" or c == '"') and inStr == False):
            openChar = c;
            inStr = True;
            openIndex = charIndex
        if(c == openChar and (charIndex - openIndex > 0)):
            inStr = False;
        
        if(c == " " and (not inStr) and charIndex > 3):
            parseArg0 = line[ps:(pe-ps)];
            break;
        else:
            pe += 1;
            charIndex += 1;

    ps = pe-ps+1;
    pe = ps;
    charMax = ps;
    charIndex = 0;
    openChar = " ";
    openIndex = len(line);
    inStr = False;
    
    for c in line:
        if(openChar == " " and (c == "'" or c == '"') and inStr == False and charIndex >= charMax):
            openChar = c;
            inStr = True;
            openIndex = charIndex
            
        if(c == openChar and (charIndex - openIndex > 0)  and charIndex > charMax):
            inStr = False;
        
        if(c == " " and (not inStr) and charIndex > 3 and charIndex > charMax):
            parseArg1 = line[ps:(pe-ps)];
            break;
        else:
            pe += 1;
            charIndex += 1;

    ps = pe-ps+1;
    pe = ps;
    
    for c in line:
        if(c == ";"):
            parseArg2 = line[ps:pe-ps];
            break;
        else:
            pe += 1;

def parseLineArg5(line):
    #parses arguments from a line assuming that it has two arguments.
    #cmd x y;
    global parseArg0, parseArg1, parseArg2, parseArg3, parseArg4;
    ps = 4;
    pe = 4;
    charIndex = 0;
    inStr = False;
    openChar = " ";
    openIndex = len(line);
    for c in line:
        if(openChar == " " and (c == "'" or c == '"') and inStr == False):
            openChar = c;
            inStr = True;
            openIndex = charIndex
        if(c == openChar and (charIndex - openIndex > 0)):
            inStr = False;
        
        if(c == " " and (not inStr) and charIndex > 3):
            parseArg0 = line[ps:(pe-ps)];
            break;
        else:
            pe += 1;
            charIndex += 1;

    ps = pe-ps+1;
    pe = ps;
    charMax = ps;
    charIndex = 0;
    openChar = " ";
    openIndex = len(line);
    inStr = False;
    
    for c in line:
        if(openChar == " " and (c == "'" or c == '"') and inStr == False and charIndex >= charMax):
            openChar = c;
            inStr = True;
            openIndex = charIndex
            
        if(c == openChar and (charIndex - openIndex > 0)  and charIndex > charMax):
            inStr = False;
        
        if(c == " " and (not inStr) and charIndex > 3 and charIndex > charMax):
            parseArg1 = line[ps:(pe-ps)];
            break;
        else:
            pe += 1;
            charIndex += 1;

    ps = pe-ps+1;
    pe = ps;
    charMax = ps;
    charIndex = 0;
    openChar = " ";
    openIndex = len(line);
    inStr = False;
    
    for c in line:
        if(openChar == " " and (c == "'" or c == '"') and inStr == False and charIndex >= charMax):
            openChar = c;
            inStr = True;
            openIndex = charIndex
            
        if(c == openChar and (charIndex - openIndex > 0)  and charIndex > charMax):
            inStr = False;
        
        if(c == " " and (not inStr) and charIndex > 3 and charIndex > charMax):
            parseArg2 = line[ps:(pe-ps)];
            break;
        else:
            pe += 1;
            charIndex += 1;

    ps = pe-ps+1;
    pe = ps;
    charMax = ps;
    charIndex = 0;
    openChar = " ";
    openIndex = len(line);
    inStr = False;
    
    for c in line:
        if(openChar == " " and (c == "'" or c == '"') and inStr == False and charIndex >= charMax):
            openChar = c;
            inStr = True;
            openIndex = charIndex
            
        if(c == openChar and (charIndex - openIndex > 0)  and charIndex > charMax):
            inStr = False;
        
        if(c == " " and (not inStr) and charIndex > 3 and charIndex > charMax):
            parseArg3 = line[ps:(pe-ps)];
            break;
        else:
            pe += 1;
            charIndex += 1;

    ps = pe-ps+1;
    pe = ps;
    
    for c in line:
        if(c == ";"):
            parseArg4 = line[ps:pe-ps];
            break;
        else:
            pe += 1;
            
#returns what list the given variable is stored in
def getList(varname):
    global sysVars, realVars, stringVars, boolVars;99
    lst = {};
    if(varname in sysVars):
        lst = sysVars;
    elif(varname in realVars):
        lst = realVars;
    elif(varname in stringVars):
        lst = realVars;
    elif(varname in boolVars):
        lst = boolVars;
    return lst;

# parseGetValue will return the actual value of a parsed argument
# if the argument is @firstname it will look for the variable "firstname"
# and return it's value, if it is passed something else, it will attempt to determine
# what type the variable is and return that
#
# TODO: make it recognize "" and '' as requirements for strings. <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
def parseGetValue(vl):
    global sysVars, realVars, stringVars, boolVars;
    if(vl[0] == '@'):
        #it is a variable
        varname = vl[1:len(vl)];
        lst = {};
        if(varname in sysVars):
            return sysVars[varname];
        elif(varname in realVars):
            return realVars[varname];
        elif(varname in stringVars):
            return stringVars[varname];
        elif(varname in boolVars):
            return boolVars[varname];
        return -1;
    else:
        #it is a value
        #first try a real value, if not, try a string
        if '.' in vl:
            try:
                return float(vl);
            except:
                pass;
        else:
            if not '~' in vl:
                try:
                    return int(vl);
                except:
                    pass;
            else:
                vl = vl[1:len(vl)];
                return currentLine + int(vl) + 1;
        ret = str(vl);
        if(("'" in ret) or ('"' in ret)):
            ret = ret[1:len(ret)-1];
        return ret;

# this function is what actually parses the command and calles the coresponding function
# you pass run an array of commands/strings and it will select the string at index currentLine
# to parse. if the function returns True, it executed/parsed correctly and you are good to continue
# the loop, if it returns False, something went wrong so we need to end the program.
def run(lineList):
    global parseArg0, parseArg1, parseArg2, parseArg3, parseArg4, currentLine, sysVars;
    cont = True;
    
    if(sysVars['_DEBUG_'] == 1):
        print(currentLine+1);
    
    if(not len(lineList) > currentLine):
        return False;
    
    parseLine = lineList[currentLine];
    if(parseLine[0] == "/"):
        currentLine += 1;
        return True;

    if(parseLine == "\n" or parseLine == " \n"):
        currentLine += 1;
        return True;
    
    cmd = parseLine[0:3];
    
    if(cmd == "def"):
        parseLineArg3(parseLine);
        vl = parseGetValue(parseArg2);
        success = fasmDef(parseArg0, parseArg1, vl);
        if(success == -1):
            return False;
    elif(cmd == "asn"):
        parseLineArg2(parseLine);
        vl = parseGetValue(parseArg1);
        success = fasmAsn(parseArg0, vl);
        if(not success):
            return False;
    elif(cmd == "add"):
        parseLineArg2(parseLine);
        success = fasmAdd(parseGetValue(parseArg0), parseGetValue(parseArg1));
        if(success == -1):
            return False;
    elif(cmd == "sub"):
        parseLineArg2(parseLine);
        success = fasmSub(parseGetValue(parseArg0), parseGetValue(parseArg1));
        if(success == -1):
            return False;
    elif(cmd == "mul"):
        parseLineArg2(parseLine);
        success = fasmMul(parseGetValue(parseArg0), parseGetValue(parseArg1));
        if(success == -1):
            return False;
    elif(cmd == "div"):
        parseLineArg2(parseLine);
        success = fasmDiv(parseGetValue(parseArg0), parseGetValue(parseArg1));
        if(success == -1):
            return False;
    elif(cmd == "mod"):
        parseLineArg2(parseLine);
        success = fasmMod(parseGetValue(parseArg0), parseGetValue(parseArg1));
        if(success == -1):
            return False;
    elif(cmd == "bit"):
        parseLineArg3(parseLine);
        success = fasmBit(parseGetValue(parseArg0), parseGetValue(parseArg1), parseArg2);
        if(success == -1):
            return False;
    elif(cmd == "jmp"):
        parseLineArg1(parseLine);
        success = fasmJmp(parseGetValue(parseArg0));
        if(success == -1):
            return False;
    elif(cmd == "imp"):
        parseLineArg1(parseLine);
        success = fasmImp(parseGetValue(parseArg0));
        if(success == -1):
            return False;
    elif(cmd == "mdl"):
        parseLineArg1(parseLine);
        success = fasmMdl(parseGetValue(parseArg0));
        if(success == -1):
            return False;
    elif(cmd == "del"):
        parseLineArg1(parseLine);
        success = fasmDel(parseArg0);
        if(success == -1):
            return False;
    elif(cmd == "pdf"):
        parseLineArg1(parseLine);
        success = fasmPdf(parseArg0);
        if(success == -1):
            return False;
    elif(cmd == "exe"):
        parseLineArg1(parseLine);
        success = fasmExe(parseArg0);
        if(success == -1):
            return False;
    elif(cmd == "chk"):
        parseLineArg5(parseLine);
        success = fasmChk(parseGetValue(parseArg0), parseGetValue(parseArg1), parseArg2, parseGetValue(parseArg3), parseGetValue(parseArg4));
        if(success == -1):
            return False;
    else:
        print("Unknown Command " + cmd);
        return False;
    currentLine += 1;
    return True;

# this is the command that starts the whole program. you call main() and it will start the rest.
def main():
    global programLines;
    init();
    while(True):
        if(not run(programLines)):
           break;
    return 0;

# start program!
main();

# debug de-init
# there is a system variable _DEBUG_ the program can set and if it is true, we are going to display
# the heap/stack/dictionaries of the program when it ended.
# this will display all variables, values, and modules that were used and not deleted.
if(sysVars["_DEBUG_"] == 1):
    print("Program Ended, [_DEBUG_]=true, currentHeap:");
    print("s:", sysVars);
    print("R:", realVars);
    print("S:", stringVars);
    print("B:", boolVars);
    print("M:", modules);
