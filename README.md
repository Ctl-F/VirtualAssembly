Roughly 4 years ago I made a python interpreter for a virtual assembly language that I dubbed FASM
The result was...unique. Now, partially out of boredom, and partially out of a desire to see how
much I have improved over 4 years I set out to make a similar language, this time in c++. This
is the result.